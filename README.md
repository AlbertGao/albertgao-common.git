# Utilities Commonly used by Albert.Gao

---

## Homepage

- [Commonjs Version](https://www.npmjs.com/package/albert.gao)
- [ES6 Version](https://www.npmjs.com/package/albert.gao-es)

## Extras

- [Python Package](https://www.npmjs.com/package/albertgao_python) which has similar functionality as this library.

## API

There's too much information, so please refer to the [API documentation](./docs/modules.md).

## Installation

- If you are using [Yarn](https://yarnpkg.com/), you can simply run
  ```sh
  yarn add albert.gao # if you are using CommonJS
  yarn add albert.gao-es # if you are using ES6
  ```
- If you are using [Npm](https://www.npmjs.com/), you can simply run
  ```sh
   npm install albert.gao # if you are using CommonJS
   npm install albert.gao-es # if you are using ES6
  ```
- If you need to install devDependencies like "eslint" , "typescript", "vite", you can run

  ```sh
  npm install albert.gao.dev --save-dev # if you are using npm
  yarn add albert.gao.dev --D # if you are using yarn
  ```

  <details>
    <summary>which will install the following devDependencies:</summary>

  - @rushstack/eslint-patch
    - ^1.1.2",
  - @types/connect-history-api-fallback
    - ^1.3.5",
  - @types/express
    - ^4.17.13",
  - @types/fs-extra
    - ^9.0.13",
  - @types/inquirer
    - ^8.2.1",
  - @types/jquery
    - ^3.5.14",
  - @types/klaw-sync
    - ^6.0.1",
  - @types/lodash
    - ^4.14.182",
  - @types/node
    - ^17.0.30",
  - @types/nodemon
    - ^1.19.1",
  - @types/ws
    - ^8.5.3",
  - @typescript-eslint/parser
    - ^5.21.0",
  - @vitejs/plugin-legacy
    - ^1.8.0",
  - @vitejs/plugin-vue
    - ^2.3.1",
  - @vitejs/plugin-vue-jsx
    - ^1.3.9",
  - @vue/eslint-config-prettier
    - ^7.0.0",
  - axios
    - ^0.27.2",
  - chalk
    - 4",
  - electron
    - ^18.2.0",
  - electron-builder
    - ^23.0.3",
  - electron-hmr
    - ^1.1.7",
  - eslint
    - ^8.14.0",
  - eslint-config-prettier
    - ^8.5.0",
  - eslint-import-resolver-alias
    - ^1.1.2",
  - eslint-plugin-import
    - ^2.26.0",
  - eslint-plugin-prettier
    - ^4.0.0",
  - eslint-plugin-promise
    - ^6.0.0",
  - eslint-plugin-vue
    - ^8.7.1",
  - nodemon
    - ^2.0.16",
  - pinia
    - ^2.0.13",
  - pm2
    - ^5.2.0",
  - prettier
    - ^2.6.1",
  - sass
    - ^1.51.0",
  - typedoc
    - ^0.22.15",
  - typedoc-plugin-markdown
    - ^3.12.1",
  - typescript
    - ^4.6.4",
  - unplugin-auto-import
    - ^0.7.0",
  - unplugin-vue-components
    - ^0.19.1",
  - vite
    - ^2.9.6",
  - vue-eslint-parser
    - ^8.3.0",
  - vue-router
    - ^4.0.14"
    </details>

## Usage

- Fully import

```typescript
import * as gao from "albert.gao"; // Fully import
```

- Import part of the library

```typescript
import { keep0s } from "albert.gao/common"; // Import part of the library
```


## Do Not Update

- inquirer @8
- nanoid @3