function myEslint(atPath) {
  return {
    env: {
      browser: true,
      commonjs: true,
      es2021: true,
      node: true,
    },
    plugins: ["prettier", "import"],
    extends: [
      "@vue/eslint-config-prettier",
      "eslint:recommended",
      "plugin:promise/recommended",
      "plugin:vue/vue3-recommended",
      "plugin:prettier/recommended",
      "prettier",
      "plugin:promise/recommended",
    ],
    parser: "vue-eslint-parser",
    parserOptions: {
      ecmaVersion: 13,
      sourceType: "module",
    },
    rules: {
      "prettier/prettier": [
        "warn",
        {
          endOfLine: "auto",
        },
      ],
      endOfLine: 0,
      "vue/multi-word-component-names": [
        "warn",
        {
          ignores: ["home", /\[.*\]/.source],
        },
      ],
      "vue/valid-attribute-name": "off",
      "import/no-duplicates": ["off", { considerQueryString: true }],
    },
    settings: {
      "import/resolver": {
        alias: {
          map: [
            ["@", atPath],
            ["~", atPath],
          ],
          extensions: [
            ".ts",
            ".js",
            ".jsx",
            ".tsx",
            ".json",
            ".vue",
            ".scss",
            ".css",
            ".sass",
          ],
        },
      },
    },
    overrides: [
      {
        files: ["*.ts"],
        parser: "@typescript-eslint/parser",
        rules: {
          "no-unused-vars": "off",
          "no-undef": "off",
        },
      },
      {
        files: ["*.vue"],
        globals: {
          defineProps: "readonly",
          defineEmits: "readonly",
        },
        parserOptions: {
          parser: {
            // Script parser for `<script lang="ts">`
            ts: "@typescript-eslint/parser",
          },
        },
        rules: {
          "vue/multi-word-component-names": "off",
          "no-undef": "off",
        },
      },
      {
        files: ["*.js"],
        parser: "espree",
      },
    ],
    globals: {},
  };
}

module.exports = myEslint;
