// typedoc --plugin typedoc-plugin-markdown --out docs src/index.ts

let cp = require("child_process");
let Fs = require("fs-extra");
let Path = require("path");

function exec(command) {
  return new Promise((resolve) =>
    cp.exec(command, resolve).stdout.pipe(process.stdout)
  );
}
(async () => {
  await exec(
    "typedoc --plugin typedoc-plugin-markdown --out docs src/index.ts"
  );
  // Fs.copySync(
  //   Path.resolve(__dirname, "../docs"),
  //   Path.resolve(__dirname, "../publish"),
  //   { overwrite: true }
  // );
  Fs.copy(
    Path.resolve(__dirname, "../docs/modules.md"),
    Path.resolve(__dirname, "../publish/README.md"),
    {
      overwrite: true,
    }
  );
  // Fs.copySync(
  //   Path.resolve(__dirname, "../docs"),
  //   Path.resolve(__dirname, "../publish-es"),
  //   { overwrite: true }
  // );
  Fs.copy(
    Path.resolve(__dirname, "../docs/modules.md"),
    Path.resolve(__dirname, "../publish-es/README.md"),
    {
      overwrite: true,
    }
  );
})();
