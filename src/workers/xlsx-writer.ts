import { WriterResult } from "./json-writer";
import { killExcel, writeABook } from "../node/misc";
import Fs from "fs-extra";
let path = "";
/**
 * 用于向主进程发送数据
 */
function postMsg(...args: any[]) {
  //@ts-ignore
  process.send(...args);
}
export interface XlsxWriterArg0 {
  path?: string;
  json?: Object[] | any[][];
  id?: string;
}
/**
 * @param root0
 * @param root0.path
 * @param root0.json
 * @param root0.id
 */
function handleData({ path: filePath, json, id }: XlsxWriterArg0) {
  filePath && (path = filePath);
  try {
    Fs.removeSync(path);
  } catch (error) {
    killExcel();
  }
  if (json) {
    writeABook(path, json);
  }
  let result: WriterResult = { path: filePath, complete: true, id };
  postMsg(result);
}

/**
 * 用于接收主进程发来的数据
 */
process.on("message", handleData);
