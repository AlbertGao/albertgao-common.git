import Fs from "fs-extra";
let path = "";

export interface JsonWriterArg0 {
  path?: string;
  json?: any;
  id?: string;
}
export interface WriterResult {
  complete: boolean;
  path?: string;
  id?: string;
}
/**
 * 用于向主进程发送数据
 */
function postMsg(...args: any[]) {
  // @ts-ignore
  process.send(...args);
}
/**
 * @param root0
 * @param root0.path
 * @param root0.json
 * @param root0.id
 */
function handleData({ path: filePath, json, id }: JsonWriterArg0) {
  filePath && (path = filePath);
  json && Fs.outputJSONSync(path, json, { spaces: "\t" });
  postMsg({ path: filePath, complete: true, id });
}

/**
 * 用于接收主进程发来的数据
 */
process.on("message", handleData);
