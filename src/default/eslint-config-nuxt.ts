import { __projectDir } from "../node/misc.js";
export const vueWebPath = __projectDir;

export const eslintNuxt3 = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
    node: true,
  },
  plugins: ["prettier", "import"],
  extends: [
    "@vue/eslint-config-prettier",
    "eslint:recommended",
    "plugin:promise/recommended",
    "plugin:vue/vue3-recommended",
    "plugin:prettier/recommended",
    "prettier",
    "plugin:promise/recommended",
  ],
  parser: "vue-eslint-parser",
  parserOptions: {
    ecmaVersion: 13,
    sourceType: "module",
  },
  rules: {
    "prettier/prettier": [
      "warn",
      {
        endOfLine: "auto",
      },
    ],
    endOfLine: 0,
    "vue/multi-word-component-names": [
      "error",
      {
        ignores: ["home", /\[.*\]/.source],
      },
    ],
    "import/no-duplicates": ["off", { considerQueryString: true }],
  },
  settings: {
    "import/resolver": {
      alias: {
        map: [
          ["@", exports.vueWebPath],
          ["~", exports.vueWebPath],
        ],
        extensions: [
          ".ts",
          ".js",
          ".jsx",
          ".json",
          ".vue",
          ".scss",
          ".css",
          ".sass",
        ],
      },
    },
  },
  overrides: [
    {
      files: ["*.ts"],
      parser: "@typescript-eslint/parser",
    },
    {
      files: ["*.vue"],
      globals: {
        defineProps: "readonly",
        defineEmits: "readonly",
      },
      parserOptions: {
        parser: {
          // Script parser for `<script lang="ts">`
          ts: "@typescript-eslint/parser",
        },
      },
    },
    {
      files: ["vue/**"],
      globals: {
        useAsyncData: "readonly",
        useCookie: "readonly",
        useError: "readonly",
        useFetch: "readonly",
        useHead: "readonly",
        useHydration: "readonly",
        useLazyAsyncData: "readonly",
        useLazyFetch: "readonly",
        useNuxtApp: "readonly",
        useRequestHeaders: "readonly",
        useRoute: "readonly",
        useRouter: "readonly",
        useState: "readonly",
        $fetch: "readonly",
        abortNavigation: "readonly",
        addRouteMiddleware: "readonly",
        clearError: "readonly",
        defineNuxtComponent: "readonly",
        defineNuxtRouteMiddleware: "readonly",
        definePageMeta: "readonly",
        navigateTo: "readonly",
        refreshNuxtData: "readonly",
        throwError: "readonly",
        // manually added
        defineNuxtPlugin: "readonly",
      },
    },
  ],
};
export default eslintNuxt3;
