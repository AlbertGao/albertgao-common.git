import { ipcRenderer } from "electron";

/**
 * 向主进程发送同步信息
 * @param route 主进程中定义的route
 * @param data 向主进程发送的data
 * @returns 返回主进程发送回的消息
 */
export function ipcRendererSync(route: string, data: any): any {
  let { sendSync } = ipcRenderer;
  return sendSync("synchronous-message", { route, data });
}
