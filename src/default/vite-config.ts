// import legacy from "@vitejs/plugin-legacy";
import vue from "@vitejs/plugin-vue";
import Path from "path";
import AutoImport from "unplugin-auto-import/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
import Components from "unplugin-vue-components/vite";
import { InlineConfig } from "vite";
import { __projectDir } from "../node/misc";
import vueJsx from "@vitejs/plugin-vue-jsx";
/**
 * vue的前端目录，默认为vue
 */
export const vueWebPath = Path.resolve(__projectDir, "vue");
/**
 * 项目默认的vite配置
 * @description "@/..." 代表vue的根目录
 * @description "~/..." 代表vue的根目录的上一级目录(Project所在目录)
 */
export const defaultConfig: InlineConfig = {
  root: vueWebPath,
  base: "/",
  resolve: {
    alias: [
      { find: "@", replacement: vueWebPath },
      { find: "~", replacement: __projectDir },
      {
        find: "components",
        replacement: Path.resolve(vueWebPath, "components"),
      },
    ],
  },
  cacheDir: Path.resolve(__projectDir, "dist/.vite"),
  plugins: [
    vue({}),
    vueJsx({}),
    AutoImport({
      resolvers: [ElementPlusResolver()],
    }),
    // legacy({
    //   targets: ["defaults", "last 2 versions", "ie >= 11"],
    //   additionalLegacyPolyfills: ["regenerator-runtime/runtime"],
    // }),
    Components({
      dirs: ["components/"],
      resolvers: [ElementPlusResolver()],
    }),
  ],
  build: {
    // 在 outDir 中生成 manifest.json
    target: "esnext",
    outDir: Path.resolve("dist/vue"),
    manifest: true,
    emptyOutDir: true,
  },
};

export default defaultConfig;
