/**
 * vite-electron-express项目的electron-builder配置
 */
export const settings = {
  appId: "albergao.com.app",
  asar: true,
  directories: {
    output: "dist/electron",
  },
  asarUnpack: ["node_modules/esbuild", "node_modules/esbuild-windows-64"],
  files: [
    "**/*",
    `dist/vue`, // 把vue保留
    `backend.js`, // 把backend.js保留
    // 根目录内容-------------------
    "!.nuxt",
    "!.dockerignore",
    "!.Dockerfile",
    "!.eslintrc.js",
    "!ecosystem.config.js",
    "!electron-builder.js",
    "!electron-mirror-server.ts",
    "!nuxt.config.ts",
    "!package.json",
    "!README.md",
    "!tsconfig.json",
    "!LICENSE",
    "!shared",
    `!vue`, //删除vue部分
    `!dist/.vite`, //删除vite缓存
    `!dev-tools`, //删除工具部分
    `!backend`, // 把backend刨除
    `!dist/electron`, // 把database刨除
    `!database`, // 把database刨除
    `!docker-debian`, // 把database刨除
    `!**/.vscode/*`, // 把database刨除
    "!**/node_modules/*/{CHANGELOG.md,README.md,README,readme.md,readme}",
    "!**/node_modules/*/{test,__tests__,tests,powered-test,example,examples}",
    "!**/node_modules/*.d.ts",
    "!**/node_modules/.bin",
    "!**/*.{iml,o,hprof,orig,pyc,pyo,rbc,swp,csproj,sln,xproj}",
    "!.editorconfig",
    "!**/._*",
    "!**/{.DS_Store,.git,.hg,.svn,CVS,RCS,SCCS,.gitignore,.gitattributes}",
    "!**/{__pycache__,thumbs.db,.flowconfig,.idea,.vs,.nyc_output}",
    "!**/{appveyor.yml,.travis.yml,circle.yml}",
    "!**/{npm-debug.log,yarn.lock,.yarn-integrity,.yarn-metadata.json}",
  ],
  win: {
    target: [
      {
        target: "7z",
        arch: ["x64", "ia32"],
      },
    ],
  },
};
export default settings;
