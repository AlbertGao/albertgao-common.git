/**
 * 默认的Express.Router所用的Routes
 */
export enum DefaultRoutes {
  login = "/api/user/log-in",
  logout = "/api/user/log-out",
  register = "/api/user/register",
  userInfo = "/api/user/info",
}

export default DefaultRoutes;
