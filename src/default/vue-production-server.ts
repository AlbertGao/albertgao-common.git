import history from "connect-history-api-fallback";
import Express, { Router } from "express";
import Path from "path";
import { __projectDir } from "../node";
/**
 * 默认的vue-production-server
 */
export function vueProdServer(): Router {
  const { Router } = Express;
  const router = Router();
  router.use(history());
  router.use(Express.static(Path.resolve(__projectDir, "dist/vue")));
  return router;
}
export default vueProdServer();

/**
 * 根据是否为生产模式决定哪个router
 * @param isProd 是否为生产模式
 * @returns Router实例
 */
export async function vueServer(isProd: boolean): Promise<Router> {
  if (isProd) {
    return vueProdServer();
  } else {
    return await require("./vite-express-app").vueDevServer();
  }
}
