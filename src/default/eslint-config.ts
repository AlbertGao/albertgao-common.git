import { __projectDir } from "../node/misc";
import Path from "path";
export const vueWebPath = Path.resolve(__projectDir, "vue");

/**
 * vite-electron-express 项目的eslint配置
 *
 */
export const eslintDefault = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
    node: true,
  },
  plugins: ["prettier", "import"],
  extends: [
    "@vue/eslint-config-prettier",
    "eslint:recommended",
    "plugin:promise/recommended",
    "plugin:vue/vue3-recommended",
    "plugin:prettier/recommended",
    "prettier",
    "plugin:promise/recommended",
  ],
  parser: "vue-eslint-parser",
  parserOptions: {
    ecmaVersion: 13,
    sourceType: "module",
  },
  rules: {
    "prettier/prettier": [
      "warn",
      {
        endOfLine: "auto",
      },
    ],
    endOfLine: 0,
    "vue/multi-word-component-names": [
      "warn",
      {
        ignores: ["home", /\[.*\]/.source],
      },
    ],
    "vue/valid-attribute-name": "off",
    "import/no-duplicates": ["off", { considerQueryString: true }],
  },
  settings: {
    "import/resolver": {
      alias: {
        map: [
          ["@", vueWebPath],
          ["~", vueWebPath],
        ],
        extensions: [
          ".ts",
          ".js",
          ".jsx",
          ".json",
          ".vue",
          ".scss",
          ".css",
          ".sass",
        ],
      },
    },
  },
  overrides: [
    {
      files: ["*.ts"],
      parser: "@typescript-eslint/parser",
      rules: {
        "no-unused-vars": "off",
        "no-undef": "off",
      },
    },
    {
      files: ["*.vue"],
      globals: {
        defineProps: "readonly",
        defineEmits: "readonly",
      },
      parserOptions: {
        parser: {
          // Script parser for `<script lang="ts">`
          ts: "@typescript-eslint/parser",
        },
      },
      rules: {
        "vue/multi-word-component-names": "off",
      },
    },
    {
      files: ["*.js"],
      parser: "espree",
    },
  ],
  globals: {},
};

/**
 * uniapp的eslint配置
 */
export const eslintUniapp = JSON.parse(
  JSON.stringify(eslintDefault)
) as typeof eslintDefault;
eslintUniapp.globals = {
  uni: "readonly",
  plus: "readonly",
  uniIDHasRole: "readonly",
  uniIDHasPermission: "readonly",
  uniCloud: "readonly",
  getCurrentPages: "readonly",
};
eslintUniapp.settings["import/resolver"].alias.map = [["@", __projectDir]];

export default eslintDefault;
