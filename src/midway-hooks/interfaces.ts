export type ServerOptions = {
  port?: number;
  root?: string;
  /**
   * 端口模式，默认不做改变
   */
  portMode?: "change-when-occupied" | "kill-when-occupied";
};

export type ServerResult = {
  /** 经过变更后的port */
  port: number;
};
