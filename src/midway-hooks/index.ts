import { app } from "electron";
import type startProd from "./index.prod";
import type startDev from "./index.dev";
import type { ServerOptions } from "./interfaces";
export async function startServer(
  options: ServerOptions = {},
  isProduction = app?.isPackaged
) {
  const start = isProduction
    ? (require("./index.prod").default as typeof startProd)
    : (require("./index.dev").default as typeof startDev);
  return await start(options);
}
export default startServer;
