import { KitConfig } from "@midwayjs/hooks-kit";
import Path from "path";
import { defaultConfig } from "../default/vite-config";
import { PROJECT_DIR } from "../node/misc";
import _ from "lodash";

export const viteConfig = _.cloneDeep(defaultConfig);

_.merge(viteConfig, {
  build: {
    outDir: Path.resolve(PROJECT_DIR, "./dist/hooks/_client"),
  },
});

export const config: KitConfig = {
  build: {
    outDir: "./dist/hooks",
  },
  // @ts-ignore
  vite: {
    ...viteConfig,
    resolve: {
      alias: {
        "@": Path.join(PROJECT_DIR, "vue"),
        $: Path.join(PROJECT_DIR, "src"),
        "~": PROJECT_DIR,
        "~~": Path.join(PROJECT_DIR, "src/api"),
      },
    },
  },
};

export default config;
