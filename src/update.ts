import Fs from "fs-extra";
import Path from "path";
import { keep0s } from "./common";

(async () => {
  for (let packagePath of [
    Path.resolve(__dirname, "./package.json"),
    Path.resolve(__dirname, "../python/package.json"),
    Path.resolve(__dirname, "../publish-es/package.json"),
    Path.resolve(__dirname, "../albert.gao.dev/package.json"),
    Path.resolve(__dirname, "../albert.gao.db3/package.json"),
  ]) {
    const json = Fs.readJsonSync(packagePath);
    const d = new Date();
    json.version = `0.0.1-alpha.${d.getFullYear()}-${keep0s(
      d.getMonth() + 1,
      2
    )}-${keep0s(d.getDate(), 2)}.${d.getTime()}`;
    Fs.writeJsonSync(packagePath, json, { spaces: "\t" });
  }
})();

(async () => {
  let basePath = Path.resolve(__dirname, "../albert.gao.dev");
  function cb() {
    const packageJson = Path.resolve(basePath, "package.json");
    const mdPath = Path.resolve(basePath, "README.md");
    const json = Fs.readJsonSync(packageJson);
    const { dependencies } = json;
    let markdown = `# ${json.name}

This package does nothing but installing the following dependencies:
`;
    for (const key in dependencies as Object) {
      markdown += `
## ${key}

    - ${dependencies[key].replace(/^\^/, "")}`;
    }
    Fs.writeFileSync(mdPath, markdown, "utf-8");
  }
  cb();
  basePath = Path.resolve(__dirname, "../albert.gao.db3");
  cb();
})();
