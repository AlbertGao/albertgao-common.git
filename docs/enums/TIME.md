[albert.gao](../README.md) / [Exports](../modules.md) / TIME

# Enumeration: TIME

常用时间毫秒数

## Table of contents

### Enumeration Members

- [day](TIME.md#day)
- [hour](TIME.md#hour)
- [minute](TIME.md#minute)
- [month](TIME.md#month)
- [second](TIME.md#second)
- [year](TIME.md#year)

## Enumeration Members

### day

• **day** = ``86400000``

一天的毫秒数

#### Defined in

src/common/index.ts:2000

___

### hour

• **hour** = ``3600000``

一小时的毫秒数

#### Defined in

src/common/index.ts:2004

___

### minute

• **minute** = ``60000``

一分钟的毫秒数

#### Defined in

src/common/index.ts:2008

___

### month

• **month** = ``2629800000``

一个月的毫秒数, 按每年有365.25天

#### Defined in

src/common/index.ts:2016

___

### second

• **second** = ``1000``

一秒钟的毫秒数

#### Defined in

src/common/index.ts:2012

___

### year

• **year** = ``31557600000``

一年的毫秒数，按365.25天计算

#### Defined in

src/common/index.ts:2020
