[albert.gao](../README.md) / [Exports](../modules.md) / [Js](../modules/Js.md) / AnalyzeOptions

# Interface: AnalyzeOptions

[Js](../modules/Js.md).AnalyzeOptions

GetRootsOfObj函数的选项

## Table of contents

### Properties

- [baseRoute](Js.AnalyzeOptions.md#baseroute)
- [considerOrigin](Js.AnalyzeOptions.md#considerorigin)
- [considerSetAsArray](Js.AnalyzeOptions.md#considersetasarray)
- [endEles](Js.AnalyzeOptions.md#endeles)
- [endRoutes](Js.AnalyzeOptions.md#endroutes)
- [separator](Js.AnalyzeOptions.md#separator)
- [useNumber](Js.AnalyzeOptions.md#usenumber)

## Properties

### baseRoute

• `Optional` **baseRoute**: [`Route`](../modules/Js.md#route)

在此之前已经存在的route

#### Defined in

src/common/index.ts:660

___

### considerOrigin

• `Optional` **considerOrigin**: `boolean`

是否考虑将源本身加入到结果中(.route为[])，默认为false

**`Description`**

注. 使用lodash的 .get(origin, route) 获得值为undefined

#### Defined in

src/common/index.ts:652

___

### considerSetAsArray

• `Optional` **considerSetAsArray**: `boolean`

是否视Set为Array (Iterable)，默认为false

#### Defined in

src/common/index.ts:643

___

### endEles

• `Optional` **endEles**: [`StopElement`](../modules/Js.md#stopelement)[]

当寻找该位置或者该函数的返回值为true时就不向深层继续寻找了

#### Defined in

src/common/index.ts:635

___

### endRoutes

• `Optional` **endRoutes**: (`string` \| `number`)[][]

在遇到什么位置时就不往下找了 (考虑baseRoute)

#### Defined in

src/common/index.ts:639

___

### separator

• `Optional` **separator**: `string`

返回的routeStr分隔符 用来把route给join起来，默认为":""

#### Defined in

src/common/index.ts:647

___

### useNumber

• `Optional` **useNumber**: `boolean`

路径中的 "1" 之类的是否切换成number 1

#### Defined in

src/common/index.ts:656
