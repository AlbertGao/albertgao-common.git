[albert.gao](../README.md) / [Exports](../modules.md) / ReadAXlsxOptions

# Interface: ReadAXlsxOptions

## Table of contents

### Properties

- [csvToObjOptions](ReadAXlsxOptions.md#csvtoobjoptions)
- [parseOptions](ReadAXlsxOptions.md#parseoptions)
- [path](ReadAXlsxOptions.md#path)

## Properties

### csvToObjOptions

• `Optional` **csvToObjOptions**: `Partial`<[`CsvToObjArrOptions`](CsvToObjArrOptions.md)\>

csv转为Object[]时的解析参数, 传给csvToObjArr1

#### Defined in

src/node/misc.ts:76

___

### parseOptions

• `Optional` **parseOptions**: `Sheet2JSONOpts` & `ParsingOptions`

读取xlsx文件时的解析参数, 传给node-xlsx

#### Defined in

src/node/misc.ts:80

___

### path

• **path**: `string`

xlsx文件的绝对路径

#### Defined in

src/node/misc.ts:72
