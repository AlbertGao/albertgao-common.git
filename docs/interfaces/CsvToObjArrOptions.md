[albert.gao](../README.md) / [Exports](../modules.md) / CsvToObjArrOptions

# Interface: CsvToObjArrOptions

## Table of contents

### Properties

- [csv](CsvToObjArrOptions.md#csv)
- [dataStart](CsvToObjArrOptions.md#datastart)
- [splitter](CsvToObjArrOptions.md#splitter)
- [titleStart](CsvToObjArrOptions.md#titlestart)
- [titlesDictionary](CsvToObjArrOptions.md#titlesdictionary)

## Properties

### csv

• `Optional` **csv**: `string` \| `any`[][]

#### Defined in

src/common/index.ts:488

___

### dataStart

• `Optional` **dataStart**: `number`

#### Defined in

src/common/index.ts:490

___

### splitter

• `Optional` **splitter**: `string` \| `RegExp`

#### Defined in

src/common/index.ts:491

___

### titleStart

• `Optional` **titleStart**: `number`

#### Defined in

src/common/index.ts:489

___

### titlesDictionary

• `Optional` **titlesDictionary**: [`TitleTransform`](TitleTransform.md)

#### Defined in

src/common/index.ts:492
