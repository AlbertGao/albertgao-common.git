[albert.gao](../README.md) / [Exports](../modules.md) / [PaddleOcr](../modules/PaddleOcr.md) / OcrResult

# Interface: OcrResult

[PaddleOcr](../modules/PaddleOcr.md).OcrResult

## Table of contents

### Properties

- [detail](PaddleOcr.OcrResult.md#detail)
- [text](PaddleOcr.OcrResult.md#text)

## Properties

### detail

• **detail**: [`ResultRow`](PaddleOcr.ResultRow.md)[]

Ocr结果，包括位置和内容

#### Defined in

src/node/run-script.ts:242

___

### text

• **text**: `string`

Ocr识别出来的文字

#### Defined in

src/node/run-script.ts:244
