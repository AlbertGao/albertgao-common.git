[albert.gao](../README.md) / [Exports](../modules.md) / [Element](../modules/Element.md) / ElementFilter

# Interface: ElementFilter<T\>

[Element](../modules/Element.md).ElementFilter

## Type parameters

| Name |
| :------ |
| `T` |

## Table of contents

### Properties

- [text](Element.ElementFilter.md#text)
- [value](Element.ElementFilter.md#value)

## Properties

### text

• **text**: `T`

#### Defined in

src/common/index.ts:1849

___

### value

• **value**: `T`

#### Defined in

src/common/index.ts:1850
