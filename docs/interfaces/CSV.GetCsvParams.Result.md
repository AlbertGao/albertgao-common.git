[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](../modules/CSV.md) / [GetCsvParams](../modules/CSV.GetCsvParams.md) / Result

# Interface: Result

[CSV](../modules/CSV.md).[GetCsvParams](../modules/CSV.GetCsvParams.md).Result

## Hierarchy

- [`Option`](CSV.GetIndexed.Option.md)

  ↳ **`Result`**

## Table of contents

### Properties

- [availableTitles](CSV.GetCsvParams.Result.md#availabletitles)
- [data](CSV.GetCsvParams.Result.md#data)
- [len](CSV.GetCsvParams.Result.md#len)
- [minInterval](CSV.GetCsvParams.Result.md#mininterval)
- [starting](CSV.GetCsvParams.Result.md#starting)
- [titleSet](CSV.GetCsvParams.Result.md#titleset)

## Properties

### availableTitles

• **availableTitles**: `string`[]

一共都有哪些列标题(已去重)

#### Defined in

src/common/index.ts:1571

___

### data

• **data**: { `[key: string]`: (`string` \| `number`)[];  }[]

由Object组成的数组，其中Object的每个值都是数组，这样可以处理一行同列名多项数据

#### Inherited from

[Option](CSV.GetIndexed.Option.md).[data](CSV.GetIndexed.Option.md#data)

#### Defined in

src/common/index.ts:1537

___

### len

• **len**: `number`

csv的行数

#### Defined in

src/common/index.ts:1573

___

### minInterval

• **minInterval**: `number`

某个列名的最大重复次数的倒数
也就是一行的某个参数最大有多少帧

#### Defined in

src/common/index.ts:1567

___

### starting

• `Optional` **starting**: `number`

第0个index在总数组中的index

#### Inherited from

[Option](CSV.GetIndexed.Option.md).[starting](CSV.GetIndexed.Option.md#starting)

#### Defined in

src/common/index.ts:1545

___

### titleSet

• **titleSet**: `Object`

每个标题都分布在那些列

#### Index signature

▪ [key: `string`]: `number`[]

#### Inherited from

[Option](CSV.GetIndexed.Option.md).[titleSet](CSV.GetIndexed.Option.md#titleset)

#### Defined in

src/common/index.ts:1541
