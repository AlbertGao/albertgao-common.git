[albert.gao](../README.md) / [Exports](../modules.md) / ServerInstance

# Interface: ServerInstance

传入的serverSetup所用的函数
serverInstance

## Hierarchy

- **`ServerInstance`**

  ↳ [`ExpressWssResult`](ExpressWssResult.md)

## Table of contents

### Properties

- [app](ServerInstance.md#app)
- [broadcast](ServerInstance.md#broadcast)
- [httpsOptions](ServerInstance.md#httpsoptions)
- [server](ServerInstance.md#server)
- [wss](ServerInstance.md#wss)

## Properties

### app

• **app**: `Express`

Express的app

#### Defined in

src/node/network.ts:22

___

### broadcast

• `Optional` **broadcast**: (`data`: `any`) => `any`

#### Type declaration

▸ (`data`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `any` |

##### Returns

`any`

#### Defined in

src/node/network.ts:27

___

### httpsOptions

• `Optional` **httpsOptions**: ``true`` \| `HttpsOptions`

#### Defined in

src/node/network.ts:28

___

### server

• `Optional` **server**: `Server` \| `Server`<typeof `IncomingMessage`, typeof `ServerResponse`\>

#### Defined in

src/node/network.ts:25

___

### wss

• **wss**: `Server`<`WebSocket`\>

#### Defined in

src/node/network.ts:26
