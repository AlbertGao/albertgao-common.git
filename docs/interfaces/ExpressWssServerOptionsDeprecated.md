[albert.gao](../README.md) / [Exports](../modules.md) / ExpressWssServerOptionsDeprecated

# Interface: ExpressWssServerOptionsDeprecated

expressWssServer的参数

## Hierarchy

- **`ExpressWssServerOptionsDeprecated`**

  ↳ [`ExpressWssResult`](ExpressWssResult.md)

## Table of contents

### Properties

- [CORS](ExpressWssServerOptionsDeprecated.md#cors)
- [changePortWhenOccupied](ExpressWssServerOptionsDeprecated.md#changeportwhenoccupied)
- [creations](ExpressWssServerOptionsDeprecated.md#creations)
- [httpsOptions](ExpressWssServerOptionsDeprecated.md#httpsoptions)
- [killPortWhenOccupied](ExpressWssServerOptionsDeprecated.md#killportwhenoccupied)
- [logOnStart](ExpressWssServerOptionsDeprecated.md#logonstart)
- [port](ExpressWssServerOptionsDeprecated.md#port)
- [setup](ExpressWssServerOptionsDeprecated.md#setup)

## Properties

### CORS

• `Optional` **CORS**: `boolean`

是否跨域

#### Defined in

src/node/network.ts:56

___

### changePortWhenOccupied

• `Optional` **changePortWhenOccupied**: `boolean`

当port被占用时是否自动分配一个新的端口

#### Defined in

src/node/network.ts:60

___

### creations

• `Optional` **creations**: [`ServerSetupFunc`](../modules.md#serversetupfunc)[]

设置server的函数数组

#### Defined in

src/node/network.ts:44

___

### httpsOptions

• `Optional` **httpsOptions**: ``true`` \| `HttpsOptions`

https的配置，
如果是true，则使用默认的local https配置

**`Default`**

false

#### Defined in

src/node/network.ts:40

___

### killPortWhenOccupied

• `Optional` **killPortWhenOccupied**: `boolean`

当port被占用时是否自动kill

#### Defined in

src/node/network.ts:64

___

### logOnStart

• `Optional` **logOnStart**: `boolean`

在启动时是否log IP等信息

#### Defined in

src/node/network.ts:68

___

### port

• `Optional` **port**: `number`

https和http的共用端口

#### Defined in

src/node/network.ts:52

___

### setup

• `Optional` **setup**: [`ServerSetupFunc`](../modules.md#serversetupfunc)[]

设置server的函数数组

#### Defined in

src/node/network.ts:48
