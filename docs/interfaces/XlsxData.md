[albert.gao](../README.md) / [Exports](../modules.md) / XlsxData

# Interface: XlsxData

向xlsx写入的的数据对

## Table of contents

### Properties

- [pos](XlsxData.md#pos)
- [value](XlsxData.md#value)

## Properties

### pos

• **pos**: `string`

在sheet中写入数据的位置

#### Defined in

src/node/excel.ts:13

___

### value

• **value**: ``null`` \| `string` \| `number` \| (`undefined` \| `string` \| `number`)[][]

在该pos写的的数据的value

#### Defined in

src/node/excel.ts:15
