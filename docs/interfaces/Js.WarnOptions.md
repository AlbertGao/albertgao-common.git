[albert.gao](../README.md) / [Exports](../modules.md) / [Js](../modules/Js.md) / WarnOptions

# Interface: WarnOptions

[Js](../modules/Js.md).WarnOptions

fixObjArrByTemplate中的warnOptions

## Table of contents

### Properties

- [logger](Js.WarnOptions.md#logger)

## Properties

### logger

• `Optional` **logger**: (`template`: [`Report`](Js.Report.md), `actual`: [`Report`](Js.Report.md)) => `any`

#### Type declaration

▸ (`template`, `actual`): `any`

用来log错误

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `template` | [`Report`](Js.Report.md) | 在template中的原样 |
| `actual` | [`Report`](Js.Report.md) | 实际的值 |

##### Returns

`any`

#### Defined in

src/common/index.ts:739
