[albert.gao](../README.md) / [Exports](../modules.md) / [Js](../modules/Js.md) / ObjectRouteReport

# Interface: ObjectRouteReport

[Js](../modules/Js.md).ObjectRouteReport

## Table of contents

### Properties

- [arrays](Js.ObjectRouteReport.md#arrays)
- [ends](Js.ObjectRouteReport.md#ends)
- [noneEnds](Js.ObjectRouteReport.md#noneends)

## Properties

### arrays

• **arrays**: [`Report`](Js.Report.md)[]

#### Defined in

src/common/index.ts:730

___

### ends

• **ends**: [`Report`](Js.Report.md)[]

#### Defined in

src/common/index.ts:728

___

### noneEnds

• **noneEnds**: [`Report`](Js.Report.md)[]

#### Defined in

src/common/index.ts:729
