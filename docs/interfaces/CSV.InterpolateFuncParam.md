[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](../modules/CSV.md) / InterpolateFuncParam

# Interface: InterpolateFuncParam

[CSV](../modules/CSV.md).InterpolateFuncParam

## Hierarchy

- **`InterpolateFuncParam`**

  ↳ [`InterpolateFuncArg`](CSV.InterpolateFuncArg.md)

## Table of contents

### Properties

- [x0](CSV.InterpolateFuncParam.md#x0)
- [x1](CSV.InterpolateFuncParam.md#x1)
- [y0](CSV.InterpolateFuncParam.md#y0)
- [y1](CSV.InterpolateFuncParam.md#y1)

## Properties

### x0

• **x0**: `number`

第0个线性点的x坐标

#### Defined in

src/common/index.ts:1504

___

### x1

• **x1**: `number`

第1个线性点的x坐标

#### Defined in

src/common/index.ts:1508

___

### y0

• **y0**: `any`

第0个线性点的y坐标

#### Defined in

src/common/index.ts:1506

___

### y1

• **y1**: `any`

第1个线性点的y坐标

#### Defined in

src/common/index.ts:1510
