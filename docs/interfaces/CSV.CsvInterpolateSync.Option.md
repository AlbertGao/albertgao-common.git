[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](../modules/CSV.md) / [CsvInterpolateSync](../modules/CSV.CsvInterpolateSync.md) / Option

# Interface: Option

[CSV](../modules/CSV.md).[CsvInterpolateSync](../modules/CSV.CsvInterpolateSync.md).Option

## Hierarchy

- [`Option`](CSV.GetCsvParams.Option.md)

  ↳ **`Option`**

## Table of contents

### Properties

- [colSplitter](CSV.CsvInterpolateSync.Option.md#colsplitter)
- [contentStart](CSV.CsvInterpolateSync.Option.md#contentstart)
- [csvStr](CSV.CsvInterpolateSync.Option.md#csvstr)
- [interpolate](CSV.CsvInterpolateSync.Option.md#interpolate)
- [rowSplitter](CSV.CsvInterpolateSync.Option.md#rowsplitter)
- [titleIndex](CSV.CsvInterpolateSync.Option.md#titleindex)

## Properties

### colSplitter

• `Optional` **colSplitter**: `string` \| `RegExp`

每行用什么分割，默认为 ","

#### Inherited from

[Option](CSV.GetCsvParams.Option.md).[colSplitter](CSV.GetCsvParams.Option.md#colsplitter)

#### Defined in

src/common/index.ts:1560

___

### contentStart

• `Optional` **contentStart**: `number`

哪个index为content的第一行 (从0开始计算，默认为2)

#### Inherited from

[Option](CSV.GetCsvParams.Option.md).[contentStart](CSV.GetCsvParams.Option.md#contentstart)

#### Defined in

src/common/index.ts:1556

___

### csvStr

• **csvStr**: `string`

读取的csv文件内容

#### Inherited from

[Option](CSV.GetCsvParams.Option.md).[csvStr](CSV.GetCsvParams.Option.md#csvstr)

#### Defined in

src/common/index.ts:1552

___

### interpolate

• `Optional` **interpolate**: [`InterpolateFunc`](../modules/CSV.md#interpolatefunc)

插值函数, 仅在Sync中可选

#### Defined in

src/common/index.ts:1580

___

### rowSplitter

• `Optional` **rowSplitter**: `string` \| `RegExp`

每行用什么分割，默认为 \r?\n

#### Inherited from

[Option](CSV.GetCsvParams.Option.md).[rowSplitter](CSV.GetCsvParams.Option.md#rowsplitter)

#### Defined in

src/common/index.ts:1558

___

### titleIndex

• `Optional` **titleIndex**: `number`

哪个index为titles (从0开始计算，默认为0)

#### Inherited from

[Option](CSV.GetCsvParams.Option.md).[titleIndex](CSV.GetCsvParams.Option.md#titleindex)

#### Defined in

src/common/index.ts:1554
