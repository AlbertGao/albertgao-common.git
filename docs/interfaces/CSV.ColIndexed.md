[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](../modules/CSV.md) / ColIndexed

# Interface: ColIndexed

[CSV](../modules/CSV.md).ColIndexed

键为非整数，值为该列该位置对应的值

## Indexable

▪ [key: `number`]: `number` \| `string`
