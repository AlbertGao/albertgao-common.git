[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](../modules/CSV.md) / InterpolateFuncArg

# Interface: InterpolateFuncArg

[CSV](../modules/CSV.md).InterpolateFuncArg

## Hierarchy

- [`InterpolateFuncParam`](CSV.InterpolateFuncParam.md)

  ↳ **`InterpolateFuncArg`**

## Table of contents

### Properties

- [colName](CSV.InterpolateFuncArg.md#colname)
- [x](CSV.InterpolateFuncArg.md#x)
- [x0](CSV.InterpolateFuncArg.md#x0)
- [x1](CSV.InterpolateFuncArg.md#x1)
- [y0](CSV.InterpolateFuncArg.md#y0)
- [y1](CSV.InterpolateFuncArg.md#y1)

## Properties

### colName

• `Optional` **colName**: `string`

列名称

#### Defined in

src/common/index.ts:1517

___

### x

• **x**: `number`

实际的x

#### Defined in

src/common/index.ts:1515

___

### x0

• **x0**: `number`

第0个线性点的x坐标

#### Inherited from

[InterpolateFuncParam](CSV.InterpolateFuncParam.md).[x0](CSV.InterpolateFuncParam.md#x0)

#### Defined in

src/common/index.ts:1504

___

### x1

• **x1**: `number`

第1个线性点的x坐标

#### Inherited from

[InterpolateFuncParam](CSV.InterpolateFuncParam.md).[x1](CSV.InterpolateFuncParam.md#x1)

#### Defined in

src/common/index.ts:1508

___

### y0

• **y0**: `any`

第0个线性点的y坐标

#### Inherited from

[InterpolateFuncParam](CSV.InterpolateFuncParam.md).[y0](CSV.InterpolateFuncParam.md#y0)

#### Defined in

src/common/index.ts:1506

___

### y1

• **y1**: `any`

第1个线性点的y坐标

#### Inherited from

[InterpolateFuncParam](CSV.InterpolateFuncParam.md).[y1](CSV.InterpolateFuncParam.md#y1)

#### Defined in

src/common/index.ts:1510
