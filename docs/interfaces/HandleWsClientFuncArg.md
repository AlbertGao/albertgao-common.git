[albert.gao](../README.md) / [Exports](../modules.md) / HandleWsClientFuncArg

# Interface: HandleWsClientFuncArg

## Table of contents

### Properties

- [broadcast](HandleWsClientFuncArg.md#broadcast)
- [client](HandleWsClientFuncArg.md#client)
- [clients](HandleWsClientFuncArg.md#clients)
- [json](HandleWsClientFuncArg.md#json)
- [msg](HandleWsClientFuncArg.md#msg)
- [reply](HandleWsClientFuncArg.md#reply)
- [wss](HandleWsClientFuncArg.md#wss)

## Properties

### broadcast

• **broadcast**: `Function`

广播实例，广播时，内容会自动JSON.stringify

#### Defined in

src/node/network.ts:90

___

### client

• **client**: `WebSocket`

当前的client

#### Defined in

src/node/network.ts:106

___

### clients

• **clients**: `WebSocket`[]

所有的client

#### Defined in

src/node/network.ts:102

___

### json

• `Optional` **json**: `any`

收到的json, 已经过JSON.parse

#### Defined in

src/node/network.ts:98

___

### msg

• **msg**: `string`

收到的msg，未经过JSON.parse

#### Defined in

src/node/network.ts:94

___

### reply

• **reply**: (`data`: `any`) => `any`

#### Type declaration

▸ (`data`): `any`

回复函数、发送的数据会经过JSON.stringify

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `data` | `any` | 要发送的数据 |

##### Returns

`any`

#### Defined in

src/node/network.ts:110

___

### wss

• **wss**: `Server`<`WebSocket`\>

WebSocketServer实例

#### Defined in

src/node/network.ts:86
