[albert.gao](../README.md) / [Exports](../modules.md) / MyWorker

# Interface: MyWorker

## Hierarchy

- **`MyWorker`**

  ↳ [`CsvAccFuncReturns`](CsvAccFuncReturns.md)

  ↳ [`JsonWriterReturns`](JsonWriterReturns.md)

  ↳ [`XlsxWriterReturns`](XlsxWriterReturns.md)

  ↳ [`JsonAccReturns`](JsonAccReturns.md)

## Table of contents

### Properties

- [kill](MyWorker.md#kill)
- [terminate](MyWorker.md#terminate)
- [worker](MyWorker.md#worker)

## Properties

### kill

• **kill**: () => `void`

#### Type declaration

▸ (): `void`

停止该进程

##### Returns

`void`

#### Defined in

src/workers/index.ts:36

___

### terminate

• **terminate**: () => `void`

#### Type declaration

▸ (): `void`

停止该进程

##### Returns

`void`

#### Defined in

src/workers/index.ts:32

___

### worker

• **worker**: `ChildProcess`

返回worker实例

#### Defined in

src/workers/index.ts:28
