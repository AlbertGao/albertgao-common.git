[albert.gao](../README.md) / [Exports](../modules.md) / ExpressAppSetupFunc

# Interface: ExpressAppSetupFunc

## Callable

### ExpressAppSetupFunc

▸ **ExpressAppSetupFunc**(`«destructured»`): `void` \| `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `«destructured»` | [`CreateExpressWsResult`](../modules.md#createexpresswsresult) |

#### Returns

`void` \| `Promise`<`void`\>

#### Defined in

src/node/network2.ts:19
