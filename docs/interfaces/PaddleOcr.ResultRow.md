[albert.gao](../README.md) / [Exports](../modules.md) / [PaddleOcr](../modules/PaddleOcr.md) / ResultRow

# Interface: ResultRow

[PaddleOcr](../modules/PaddleOcr.md).ResultRow

## Table of contents

### Properties

- [content](PaddleOcr.ResultRow.md#content)
- [pos](PaddleOcr.ResultRow.md#pos)

## Properties

### content

• **content**: `Object`

该字符串的内容和准确度

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `accuracy` | `number` | 该字符串的准确度, 越1越准确 |
| `text` | `string` | 该字符串的内容 |

#### Defined in

src/node/run-script.ts:233

___

### pos

• **pos**: [[`Pos`](PaddleOcr.Pos.md), [`Pos`](PaddleOcr.Pos.md), [`Pos`](PaddleOcr.Pos.md), [`Pos`](PaddleOcr.Pos.md)]

该字符串的位置(四个角)

#### Defined in

src/node/run-script.ts:231
