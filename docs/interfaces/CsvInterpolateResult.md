[albert.gao](../README.md) / [Exports](../modules.md) / CsvInterpolateResult

# Interface: CsvInterpolateResult

## Table of contents

### Properties

- [csvParams](CsvInterpolateResult.md#csvparams)
- [final](CsvInterpolateResult.md#final)

## Properties

### csvParams

• **csvParams**: [`Result`](CSV.GetCsvParams.Result.md)

#### Defined in

src/node/csv-interpolate.ts:24

___

### final

• **final**: [`Row`](CSV.FinalResult.Row.md)[]

#### Defined in

src/node/csv-interpolate.ts:23
