[albert.gao](../README.md) / [Exports](../modules.md) / To60sFromSecOptions

# Interface: To60sFromSecOptions

## Table of contents

### Properties

- [base](To60sFromSecOptions.md#base)
- [colons](To60sFromSecOptions.md#colons)
- [delimiter](To60sFromSecOptions.md#delimiter)
- [digits](To60sFromSecOptions.md#digits)
- [keep0](To60sFromSecOptions.md#keep0)

## Properties

### base

• `Optional` **base**: `number`

多少进制，默认60

#### Defined in

src/common/index.ts:165

___

### colons

• `Optional` **colons**: `number`

使用几个:分割，默认为1

#### Defined in

src/common/index.ts:161

___

### delimiter

• `Optional` **delimiter**: `string`

使用什么分割，默认":"

#### Defined in

src/common/index.ts:169

___

### digits

• `Optional` **digits**: `number`

小数点后保留多少位，默认为0

#### Defined in

src/common/index.ts:157

___

### keep0

• `Optional` **keep0**: `boolean`

是否根据进制保留前面的0，默认为true

#### Defined in

src/common/index.ts:173
