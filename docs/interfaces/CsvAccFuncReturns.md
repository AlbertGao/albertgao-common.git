[albert.gao](../README.md) / [Exports](../modules.md) / CsvAccFuncReturns

# Interface: CsvAccFuncReturns

## Hierarchy

- [`MyWorker`](MyWorker.md)

  ↳ **`CsvAccFuncReturns`**

## Table of contents

### Properties

- [kill](CsvAccFuncReturns.md#kill)
- [push](CsvAccFuncReturns.md#push)
- [setTitles](CsvAccFuncReturns.md#settitles)
- [terminate](CsvAccFuncReturns.md#terminate)
- [worker](CsvAccFuncReturns.md#worker)

## Properties

### kill

• **kill**: () => `void`

#### Type declaration

▸ (): `void`

停止该进程

##### Returns

`void`

#### Inherited from

[MyWorker](MyWorker.md).[kill](MyWorker.md#kill)

#### Defined in

src/workers/index.ts:36

___

### push

• **push**: (...`lines`: `CsvLine`[]) => `Promise`<``true``\>

#### Type declaration

▸ (`...lines`): `Promise`<``true``\>

增加一行

##### Parameters

| Name | Type |
| :------ | :------ |
| `...lines` | `CsvLine`[] |

##### Returns

`Promise`<``true``\>

#### Defined in

src/workers/index.ts:47

___

### setTitles

• **setTitles**: (`titles`: `string`[]) => `void`

#### Type declaration

▸ (`titles`): `void`

设置标题行

##### Parameters

| Name | Type |
| :------ | :------ |
| `titles` | `string`[] |

##### Returns

`void`

#### Defined in

src/workers/index.ts:43

___

### terminate

• **terminate**: () => `void`

#### Type declaration

▸ (): `void`

停止该进程

##### Returns

`void`

#### Inherited from

[MyWorker](MyWorker.md).[terminate](MyWorker.md#terminate)

#### Defined in

src/workers/index.ts:32

___

### worker

• **worker**: `ChildProcess`

返回worker实例

#### Inherited from

[MyWorker](MyWorker.md).[worker](MyWorker.md#worker)

#### Defined in

src/workers/index.ts:28
