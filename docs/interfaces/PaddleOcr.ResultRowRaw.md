[albert.gao](../README.md) / [Exports](../modules.md) / [PaddleOcr](../modules/PaddleOcr.md) / ResultRowRaw

# Interface: ResultRowRaw

[PaddleOcr](../modules/PaddleOcr.md).ResultRowRaw

## Table of contents

### Properties

- [0](PaddleOcr.ResultRowRaw.md#0)
- [1](PaddleOcr.ResultRowRaw.md#1)

## Properties

### 0

• **0**: [[`PosRaw`](PaddleOcr.PosRaw.md), [`PosRaw`](PaddleOcr.PosRaw.md), [`PosRaw`](PaddleOcr.PosRaw.md), [`PosRaw`](PaddleOcr.PosRaw.md)]

该字符串的位置(四个角)

#### Defined in

src/node/run-script.ts:216

___

### 1

• **1**: `Object`

该字符串的内容和准确度

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `0` | `string` | 该字符串的内容 |
| `1` | `number` | 该字符串的准确度, 越1越准确 |

#### Defined in

src/node/run-script.ts:218
