[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](../modules/CSV.md) / [GetNearest2](../modules/CSV.GetNearest2.md) / Result1

# Interface: Result1

[CSV](../modules/CSV.md).[GetNearest2](../modules/CSV.GetNearest2.md).Result1

## Hierarchy

- [`Result0`](CSV.GetNearest2.Result0.md)

  ↳ **`Result1`**

## Table of contents

### Properties

- [key0](CSV.GetNearest2.Result1.md#key0)
- [key1](CSV.GetNearest2.Result1.md#key1)
- [value0](CSV.GetNearest2.Result1.md#value0)
- [value1](CSV.GetNearest2.Result1.md#value1)

## Properties

### key0

• **key0**: `number`

最近元素的键

#### Inherited from

[Result0](CSV.GetNearest2.Result0.md).[key0](CSV.GetNearest2.Result0.md#key0)

#### Defined in

src/common/index.ts:1591

___

### key1

• **key1**: `number`

第二近元素的键(可能不存在)

#### Defined in

src/common/index.ts:1597

___

### value0

• **value0**: `string` \| `number`

最近元素的值

#### Inherited from

[Result0](CSV.GetNearest2.Result0.md).[value0](CSV.GetNearest2.Result0.md#value0)

#### Defined in

src/common/index.ts:1593

___

### value1

• **value1**: `string` \| `number`

第二近元素的值(可能不存在)

#### Defined in

src/common/index.ts:1599
