[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](../modules/CSV.md) / [FinalResult](../modules/CSV.FinalResult.md) / Interpolated

# Interface: Interpolated

[CSV](../modules/CSV.md).[FinalResult](../modules/CSV.FinalResult.md).Interpolated

## Indexable

▪ [key: `string`]: `boolean`
