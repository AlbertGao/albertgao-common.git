[albert.gao](../README.md) / [Exports](../modules.md) / JsonAccReturns

# Interface: JsonAccReturns

## Hierarchy

- [`MyWorker`](MyWorker.md)

  ↳ **`JsonAccReturns`**

## Table of contents

### Properties

- [append](JsonAccReturns.md#append)
- [assign](JsonAccReturns.md#assign)
- [kill](JsonAccReturns.md#kill)
- [push](JsonAccReturns.md#push)
- [terminate](JsonAccReturns.md#terminate)
- [worker](JsonAccReturns.md#worker)

## Properties

### append

• **append**: [`MajorFuncJsonAcc`](../modules.md#majorfuncjsonacc)

#### Defined in

src/workers/index.ts:188

___

### assign

• **assign**: [`MajorFuncJsonAcc`](../modules.md#majorfuncjsonacc)

#### Defined in

src/workers/index.ts:186

___

### kill

• **kill**: () => `void`

#### Type declaration

▸ (): `void`

停止该进程

##### Returns

`void`

#### Inherited from

[MyWorker](MyWorker.md).[kill](MyWorker.md#kill)

#### Defined in

src/workers/index.ts:36

___

### push

• **push**: [`MajorFuncJsonAcc`](../modules.md#majorfuncjsonacc)

#### Defined in

src/workers/index.ts:187

___

### terminate

• **terminate**: () => `void`

#### Type declaration

▸ (): `void`

停止该进程

##### Returns

`void`

#### Inherited from

[MyWorker](MyWorker.md).[terminate](MyWorker.md#terminate)

#### Defined in

src/workers/index.ts:32

___

### worker

• **worker**: `ChildProcess`

返回worker实例

#### Inherited from

[MyWorker](MyWorker.md).[worker](MyWorker.md#worker)

#### Defined in

src/workers/index.ts:28
