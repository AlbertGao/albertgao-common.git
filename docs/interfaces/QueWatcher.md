[albert.gao](../README.md) / [Exports](../modules.md) / QueWatcher

# Interface: QueWatcher

## Callable

### QueWatcher

▸ **QueWatcher**(`arg`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `arg` | [`QueProgress`](QueProgress.md) |

#### Returns

`void`

#### Defined in

src/workers/index.ts:258
