[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](../modules/CSV.md) / [GetNearest2](../modules/CSV.GetNearest2.md) / Result0

# Interface: Result0

[CSV](../modules/CSV.md).[GetNearest2](../modules/CSV.GetNearest2.md).Result0

## Hierarchy

- **`Result0`**

  ↳ [`Result1`](CSV.GetNearest2.Result1.md)

## Table of contents

### Properties

- [key0](CSV.GetNearest2.Result0.md#key0)
- [value0](CSV.GetNearest2.Result0.md#value0)

## Properties

### key0

• **key0**: `number`

最近元素的键

#### Defined in

src/common/index.ts:1591

___

### value0

• **value0**: `string` \| `number`

最近元素的值

#### Defined in

src/common/index.ts:1593
