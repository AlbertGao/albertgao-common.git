[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](../modules/CSV.md) / [GetCsvParams](../modules/CSV.GetCsvParams.md) / Option

# Interface: Option

[CSV](../modules/CSV.md).[GetCsvParams](../modules/CSV.GetCsvParams.md).Option

## Hierarchy

- **`Option`**

  ↳ [`Option`](CSV.CsvInterpolateSync.Option.md)

## Table of contents

### Properties

- [colSplitter](CSV.GetCsvParams.Option.md#colsplitter)
- [contentStart](CSV.GetCsvParams.Option.md#contentstart)
- [csvStr](CSV.GetCsvParams.Option.md#csvstr)
- [rowSplitter](CSV.GetCsvParams.Option.md#rowsplitter)
- [titleIndex](CSV.GetCsvParams.Option.md#titleindex)

## Properties

### colSplitter

• `Optional` **colSplitter**: `string` \| `RegExp`

每行用什么分割，默认为 ","

#### Defined in

src/common/index.ts:1560

___

### contentStart

• `Optional` **contentStart**: `number`

哪个index为content的第一行 (从0开始计算，默认为2)

#### Defined in

src/common/index.ts:1556

___

### csvStr

• **csvStr**: `string`

读取的csv文件内容

#### Defined in

src/common/index.ts:1552

___

### rowSplitter

• `Optional` **rowSplitter**: `string` \| `RegExp`

每行用什么分割，默认为 \r?\n

#### Defined in

src/common/index.ts:1558

___

### titleIndex

• `Optional` **titleIndex**: `number`

哪个index为titles (从0开始计算，默认为0)

#### Defined in

src/common/index.ts:1554
