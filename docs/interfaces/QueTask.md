[albert.gao](../README.md) / [Exports](../modules.md) / QueTask

# Interface: QueTask<Result\>

## Type parameters

| Name |
| :------ |
| `Result` |

## Table of contents

### Properties

- [func](QueTask.md#func)
- [id](QueTask.md#id)
- [index](QueTask.md#index)
- [then](QueTask.md#then)

## Properties

### func

• **func**: () => `Promise`<`Result`\>

#### Type declaration

▸ (): `Promise`<`Result`\>

##### Returns

`Promise`<`Result`\>

#### Defined in

src/workers/index.ts:236

___

### id

• `Optional` **id**: `string`

#### Defined in

src/workers/index.ts:237

___

### index

• `Optional` **index**: `number`

#### Defined in

src/workers/index.ts:238

___

### then

• `Optional` **then**: (`result`: `Result`) => `void`

#### Type declaration

▸ (`result`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `result` | `Result` |

##### Returns

`void`

#### Defined in

src/workers/index.ts:239
