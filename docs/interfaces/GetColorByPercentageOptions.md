[albert.gao](../README.md) / [Exports](../modules.md) / GetColorByPercentageOptions

# Interface: GetColorByPercentageOptions

## Table of contents

### Properties

- [blue](GetColorByPercentageOptions.md#blue)
- [green](GetColorByPercentageOptions.md#green)
- [red](GetColorByPercentageOptions.md#red)

## Properties

### blue

• `Optional` **blue**: [`number`, `number`]

#### Defined in

src/common/index.ts:279

___

### green

• `Optional` **green**: [`number`, `number`]

#### Defined in

src/common/index.ts:269

___

### red

• `Optional` **red**: [`number`, `number`]

#### Defined in

src/common/index.ts:259
