[albert.gao](../README.md) / [Exports](../modules.md) / ExcelHandlerTools

# Interface: ExcelHandlerTools

用于操作Excel的工具

## Table of contents

### Properties

- [getSheet](ExcelHandlerTools.md#getsheet)
- [wb](ExcelHandlerTools.md#wb)
- [writeData](ExcelHandlerTools.md#writedata)

## Properties

### getSheet

• **getSheet**: [`GetSheet`](../modules.md#getsheet)

用于获取sheet的函数，如果无法获取，就创建并获取

#### Defined in

src/node/excel.ts:84

___

### wb

• **wb**: `Workbook`

workbook实例

#### Defined in

src/node/excel.ts:92

___

### writeData

• **writeData**: [`WriteData`](../modules.md#writedata)

向Excel文档写入内容的函数

#### Defined in

src/node/excel.ts:88
