[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](../modules/CSV.md) / [GetIndexed](../modules/CSV.GetIndexed.md) / Option

# Interface: Option

[CSV](../modules/CSV.md).[GetIndexed](../modules/CSV.GetIndexed.md).Option

## Hierarchy

- **`Option`**

  ↳ [`Result`](CSV.GetCsvParams.Result.md)

## Table of contents

### Properties

- [data](CSV.GetIndexed.Option.md#data)
- [starting](CSV.GetIndexed.Option.md#starting)
- [titleSet](CSV.GetIndexed.Option.md#titleset)

## Properties

### data

• **data**: { `[key: string]`: (`string` \| `number`)[];  }[]

由Object组成的数组，其中Object的每个值都是数组，这样可以处理一行同列名多项数据

#### Defined in

src/common/index.ts:1537

___

### starting

• `Optional` **starting**: `number`

第0个index在总数组中的index

#### Defined in

src/common/index.ts:1545

___

### titleSet

• **titleSet**: `Object`

每个标题都分布在那些列

#### Index signature

▪ [key: `string`]: `number`[]

#### Defined in

src/common/index.ts:1541
