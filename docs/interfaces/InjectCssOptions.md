[albert.gao](../README.md) / [Exports](../modules.md) / InjectCssOptions

# Interface: InjectCssOptions

## Table of contents

### Properties

- [css](InjectCssOptions.md#css)
- [filePath](InjectCssOptions.md#filepath)
- [href](InjectCssOptions.md#href)

## Properties

### css

• `Optional` **css**: `string`

要注入的css内容

#### Defined in

src/node/electron-utils.ts:127

___

### filePath

• `Optional` **filePath**: `string`

要注入的css文件地址

#### Defined in

src/node/electron-utils.ts:135

___

### href

• `Optional` **href**: `string`

要注入的hef

#### Defined in

src/node/electron-utils.ts:131
