[albert.gao](../README.md) / [Exports](../modules.md) / WsHandler

# Interface: WsHandler

## Table of contents

### Properties

- [e](WsHandler.md#e)
- [func](WsHandler.md#func)

## Properties

### e

• `Optional` **e**: ``"message"`` \| ``"error"`` \| ``"close"`` \| ``"upgrade"`` \| ``"open"`` \| ``"ping"`` \| ``"pong"`` \| ``"unexpected-response"``

#### Defined in

src/node/network.ts:118

___

### func

• `Optional` **func**: (`options`: [`HandleWsClientFuncArg`](HandleWsClientFuncArg.md)) => `any`

#### Type declaration

▸ (`options`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`HandleWsClientFuncArg`](HandleWsClientFuncArg.md) |

##### Returns

`any`

#### Defined in

src/node/network.ts:127
