[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](../modules/CSV.md) / [StartInterpolation](../modules/CSV.StartInterpolation.md) / Option

# Interface: Option

[CSV](../modules/CSV.md).[StartInterpolation](../modules/CSV.StartInterpolation.md).Option

## Table of contents

### Properties

- [allFrames](CSV.StartInterpolation.Option.md#allframes)
- [indexed](CSV.StartInterpolation.Option.md#indexed)
- [interpolate](CSV.StartInterpolation.Option.md#interpolate)

## Properties

### allFrames

• **allFrames**: `number`[]

所有秒数的数组

#### Defined in

src/common/index.ts:1524

___

### indexed

• **indexed**: [`Indexed`](CSV.Indexed.md)

类似于 { col0: { 0 : 1, 0.25: 4} } 这样组成的字典，第一层键为列名称，第二层键为非整数数字

#### Defined in

src/common/index.ts:1526

___

### interpolate

• `Optional` **interpolate**: [`InterpolateFunc`](../modules/CSV.md#interpolatefunc)

用于计算插值的函数

#### Defined in

src/common/index.ts:1528
