[albert.gao](../README.md) / [Exports](../modules.md) / [Excel](../modules/Excel.md) / Position

# Interface: Position

[Excel](../modules/Excel.md).Position

## Table of contents

### Properties

- [cell](Excel.Position.md#cell)
- [sheet](Excel.Position.md#sheet)
- [x](Excel.Position.md#x)
- [y](Excel.Position.md#y)

## Properties

### cell

• **cell**: `string`

#### Defined in

src/common/index.ts:1378

___

### sheet

• `Optional` **sheet**: `string`

#### Defined in

src/common/index.ts:1379

___

### x

• **x**: `number`

#### Defined in

src/common/index.ts:1376

___

### y

• **y**: `number`

#### Defined in

src/common/index.ts:1377
