[albert.gao](../README.md) / [Exports](../modules.md) / JsonWriterReturns

# Interface: JsonWriterReturns

## Hierarchy

- [`MyWorker`](MyWorker.md)

  ↳ **`JsonWriterReturns`**

## Table of contents

### Properties

- [kill](JsonWriterReturns.md#kill)
- [output](JsonWriterReturns.md#output)
- [terminate](JsonWriterReturns.md#terminate)
- [worker](JsonWriterReturns.md#worker)
- [write](JsonWriterReturns.md#write)

## Properties

### kill

• **kill**: () => `void`

#### Type declaration

▸ (): `void`

停止该进程

##### Returns

`void`

#### Inherited from

[MyWorker](MyWorker.md).[kill](MyWorker.md#kill)

#### Defined in

src/workers/index.ts:36

___

### output

• **output**: [`JsonWriterMajorWrite`](../modules.md#jsonwritermajorwrite)

#### Defined in

src/workers/index.ts:88

___

### terminate

• **terminate**: () => `void`

#### Type declaration

▸ (): `void`

停止该进程

##### Returns

`void`

#### Inherited from

[MyWorker](MyWorker.md).[terminate](MyWorker.md#terminate)

#### Defined in

src/workers/index.ts:32

___

### worker

• **worker**: `ChildProcess`

返回worker实例

#### Inherited from

[MyWorker](MyWorker.md).[worker](MyWorker.md#worker)

#### Defined in

src/workers/index.ts:28

___

### write

• **write**: [`JsonWriterMajorWrite`](../modules.md#jsonwritermajorwrite)

#### Defined in

src/workers/index.ts:87
