[albert.gao](../README.md) / [Exports](../modules.md) / ExpressWssResult

# Interface: ExpressWssResult

expressWssServer的返回结果

## Hierarchy

- [`ExpressWssServerOptionsDeprecated`](ExpressWssServerOptionsDeprecated.md)

- [`ServerInstance`](ServerInstance.md)

  ↳ **`ExpressWssResult`**

## Table of contents

### Properties

- [CORS](ExpressWssResult.md#cors)
- [app](ExpressWssResult.md#app)
- [broadcast](ExpressWssResult.md#broadcast)
- [changePortWhenOccupied](ExpressWssResult.md#changeportwhenoccupied)
- [creations](ExpressWssResult.md#creations)
- [httpsOptions](ExpressWssResult.md#httpsoptions)
- [killPortWhenOccupied](ExpressWssResult.md#killportwhenoccupied)
- [logOnStart](ExpressWssResult.md#logonstart)
- [port](ExpressWssResult.md#port)
- [proxyPort](ExpressWssResult.md#proxyport)
- [server](ExpressWssResult.md#server)
- [setup](ExpressWssResult.md#setup)
- [wss](ExpressWssResult.md#wss)

## Properties

### CORS

• `Optional` **CORS**: `boolean`

是否跨域

#### Inherited from

[ExpressWssServerOptionsDeprecated](ExpressWssServerOptionsDeprecated.md).[CORS](ExpressWssServerOptionsDeprecated.md#cors)

#### Defined in

src/node/network.ts:56

___

### app

• **app**: `Express`

Express的app

#### Inherited from

[ServerInstance](ServerInstance.md).[app](ServerInstance.md#app)

#### Defined in

src/node/network.ts:22

___

### broadcast

• `Optional` **broadcast**: (`data`: `any`) => `any`

#### Type declaration

▸ (`data`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `any` |

##### Returns

`any`

#### Inherited from

[ServerInstance](ServerInstance.md).[broadcast](ServerInstance.md#broadcast)

#### Defined in

src/node/network.ts:27

___

### changePortWhenOccupied

• `Optional` **changePortWhenOccupied**: `boolean`

当port被占用时是否自动分配一个新的端口

#### Inherited from

[ExpressWssServerOptionsDeprecated](ExpressWssServerOptionsDeprecated.md).[changePortWhenOccupied](ExpressWssServerOptionsDeprecated.md#changeportwhenoccupied)

#### Defined in

src/node/network.ts:60

___

### creations

• `Optional` **creations**: [`ServerSetupFunc`](../modules.md#serversetupfunc)[]

设置server的函数数组

#### Inherited from

[ExpressWssServerOptionsDeprecated](ExpressWssServerOptionsDeprecated.md).[creations](ExpressWssServerOptionsDeprecated.md#creations)

#### Defined in

src/node/network.ts:44

___

### httpsOptions

• `Optional` **httpsOptions**: ``true`` \| `HttpsOptions`

https的配置，
如果是true，则使用默认的local https配置

**`Default`**

false

#### Inherited from

[ServerInstance](ServerInstance.md).[httpsOptions](ServerInstance.md#httpsoptions)

#### Defined in

src/node/network.ts:40

___

### killPortWhenOccupied

• `Optional` **killPortWhenOccupied**: `boolean`

当port被占用时是否自动kill

#### Inherited from

[ExpressWssServerOptionsDeprecated](ExpressWssServerOptionsDeprecated.md).[killPortWhenOccupied](ExpressWssServerOptionsDeprecated.md#killportwhenoccupied)

#### Defined in

src/node/network.ts:64

___

### logOnStart

• `Optional` **logOnStart**: `boolean`

在启动时是否log IP等信息

#### Inherited from

[ExpressWssServerOptionsDeprecated](ExpressWssServerOptionsDeprecated.md).[logOnStart](ExpressWssServerOptionsDeprecated.md#logonstart)

#### Defined in

src/node/network.ts:68

___

### port

• `Optional` **port**: `number`

https和http的共用端口

#### Inherited from

[ExpressWssServerOptionsDeprecated](ExpressWssServerOptionsDeprecated.md).[port](ExpressWssServerOptionsDeprecated.md#port)

#### Defined in

src/node/network.ts:52

___

### proxyPort

• **proxyPort**: `number`

http和https协议共同使用的port

#### Defined in

src/node/network.ts:79

___

### server

• `Optional` **server**: `Server` \| `Server`<typeof `IncomingMessage`, typeof `ServerResponse`\>

#### Inherited from

[ServerInstance](ServerInstance.md).[server](ServerInstance.md#server)

#### Defined in

src/node/network.ts:25

___

### setup

• `Optional` **setup**: [`ServerSetupFunc`](../modules.md#serversetupfunc)[]

设置server的函数数组

#### Inherited from

[ExpressWssServerOptionsDeprecated](ExpressWssServerOptionsDeprecated.md).[setup](ExpressWssServerOptionsDeprecated.md#setup)

#### Defined in

src/node/network.ts:48

___

### wss

• **wss**: `Server`<`WebSocket`\>

#### Inherited from

[ServerInstance](ServerInstance.md).[wss](ServerInstance.md#wss)

#### Defined in

src/node/network.ts:26
