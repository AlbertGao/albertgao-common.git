[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](../modules/CSV.md) / [FinalResult](../modules/CSV.FinalResult.md) / Row

# Interface: Row

[CSV](../modules/CSV.md).[FinalResult](../modules/CSV.FinalResult.md).Row

## Indexable

▪ [key: `string`]: `number` \| `string` \| [`Interpolated`](CSV.FinalResult.Interpolated.md)

## Table of contents

### Properties

- [$interpolated](CSV.FinalResult.Row.md#$interpolated)

## Properties

### $interpolated

• **$interpolated**: [`Interpolated`](CSV.FinalResult.Interpolated.md)

都有哪些列是被interpolated的，而不是原值

#### Defined in

src/common/index.ts:1610
