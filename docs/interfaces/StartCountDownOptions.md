[albert.gao](../README.md) / [Exports](../modules.md) / StartCountDownOptions

# Interface: StartCountDownOptions

## Table of contents

### Properties

- [beforeStart](StartCountDownOptions.md#beforestart)
- [countDownMessage](StartCountDownOptions.md#countdownmessage)
- [duration](StartCountDownOptions.md#duration)
- [every](StartCountDownOptions.md#every)
- [fail](StartCountDownOptions.md#fail)
- [interval](StartCountDownOptions.md#interval)
- [showCancel](StartCountDownOptions.md#showcancel)
- [showRestart](StartCountDownOptions.md#showrestart)
- [showStart](StartCountDownOptions.md#showstart)
- [spanId](StartCountDownOptions.md#spanid)
- [success](StartCountDownOptions.md#success)

## Properties

### beforeStart

• `Optional` **beforeStart**: `Function`

是否显示"重新"

#### Defined in

src/browser/index.ts:122

___

### countDownMessage

• `Optional` **countDownMessage**: (`timeLeft`: `number`) => `string`

#### Type declaration

▸ (`timeLeft`): `string`

用于显示timeleft的函数，返回字符串

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `timeLeft` | `number` | 剩余的毫秒数 |

##### Returns

`string`

#### Defined in

src/browser/index.ts:101

___

### duration

• `Optional` **duration**: `number`

倒计时的总毫秒数

#### Defined in

src/browser/index.ts:93

___

### every

• `Optional` **every**: `Function`

每间隔都运行的函数

#### Defined in

src/browser/index.ts:134

___

### fail

• `Optional` **fail**: `Function`

倒计时取消时运行的函数

#### Defined in

src/browser/index.ts:126

___

### interval

• `Optional` **interval**: `number`

倒计时所使用的减小间隔

#### Defined in

src/browser/index.ts:97

___

### showCancel

• `Optional` **showCancel**: `boolean`

是否显示"取消"

#### Defined in

src/browser/index.ts:110

___

### showRestart

• `Optional` **showRestart**: `boolean`

是否显示"重新"

#### Defined in

src/browser/index.ts:118

___

### showStart

• `Optional` **showStart**: `boolean`

是否显示"立即"

#### Defined in

src/browser/index.ts:114

___

### spanId

• `Optional` **spanId**: `string`

倒计时内的span的id

#### Defined in

src/browser/index.ts:89

___

### success

• `Optional` **success**: `Function`

倒计时结束时运行的函数

#### Defined in

src/browser/index.ts:130
