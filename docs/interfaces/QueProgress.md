[albert.gao](../README.md) / [Exports](../modules.md) / QueProgress

# Interface: QueProgress

## Table of contents

### Properties

- [detail](QueProgress.md#detail)
- [finished](QueProgress.md#finished)
- [processing](QueProgress.md#processing)
- [progress](QueProgress.md#progress)
- [total](QueProgress.md#total)
- [waiting](QueProgress.md#waiting)

## Properties

### detail

• **detail**: \`${number}/${number} (${number}%)\`

已完成 / 总共 (百分比)

#### Defined in

src/workers/index.ts:252

___

### finished

• **finished**: `number`

有多少个func已经await了

#### Defined in

src/workers/index.ts:246

___

### processing

• **processing**: `number`

有多少个func还在运行中

#### Defined in

src/workers/index.ts:248

___

### progress

• **progress**: \`${number}%\`

已完成的项目的百分比

#### Defined in

src/workers/index.ts:250

___

### total

• **total**: `number`

总共给了多少个项目

#### Defined in

src/workers/index.ts:254

___

### waiting

• **waiting**: `number`

有多少个func还没有执行

#### Defined in

src/workers/index.ts:244
