[albert.gao](../README.md) / [Exports](../modules.md) / SecOf60sOptions

# Interface: SecOf60sOptions

## Table of contents

### Properties

- [base](SecOf60sOptions.md#base)
- [delimiter](SecOf60sOptions.md#delimiter)

## Properties

### base

• `Optional` **base**: ``60``

多少进制

#### Defined in

src/common/index.ts:114

___

### delimiter

• `Optional` **delimiter**: `string` \| `RegExp`

分隔符

#### Defined in

src/common/index.ts:118
