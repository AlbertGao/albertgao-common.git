[albert.gao](../README.md) / [Exports](../modules.md) / [Js](../modules/Js.md) / Report

# Interface: Report

[Js](../modules/Js.md).Report

## Table of contents

### Properties

- [depth](Js.Report.md#depth)
- [insideArray](Js.Report.md#insidearray)
- [instance](Js.Report.md#instance)
- [isEnd](Js.Report.md#isend)
- [path](Js.Report.md#path)
- [route](Js.Report.md#route)
- [routeIn](Js.Report.md#routein)
- [value](Js.Report.md#value)

## Properties

### depth

• **depth**: `number`

从0开始的深度，也就是route的length (考虑baseRoute)

#### Defined in

src/common/index.ts:720

___

### insideArray

• **insideArray**: `number`

嵌套在几个Array里，不考虑源本身，不考虑baseRoute，因为无法计算

#### Defined in

src/common/index.ts:716

___

### instance

• **instance**: `string`

类型 (字符串)

#### Defined in

src/common/index.ts:712

___

### isEnd

• **isEnd**: `boolean`

是否为该route的最远端

#### Defined in

src/common/index.ts:724

___

### path

• **path**: `string`

路径，用字符串表示

**`Link`**

https://www.lodashjs.com/docs/lodash.set#_setobject-path-value 的path

#### Defined in

src/common/index.ts:704

___

### route

• **route**: [`Route`](../modules/Js.md#route)

路径，用数组表示 (考虑baseRoute)

**`Link`**

https://www.lodashjs.com/docs/lodash.set#_setobject-path-value 的path

#### Defined in

src/common/index.ts:694

___

### routeIn

• **routeIn**: [`Route`](../modules/Js.md#route)

路径，用数组表示 (不考虑baseRoute)

**`Link`**

https://www.lodashjs.com/docs/lodash.set#_setobject-path-value 的path

#### Defined in

src/common/index.ts:699

___

### value

• **value**: `any`

在该route的值

#### Defined in

src/common/index.ts:708
