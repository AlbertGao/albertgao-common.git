[albert.gao](../README.md) / [Exports](../modules.md) / RunScriptArg

# Interface: RunScriptArg

runScript函数的参数

## Table of contents

### Properties

- [atDir](RunScriptArg.md#atdir)
- [copyFiles](RunScriptArg.md#copyfiles)
- [filePath](RunScriptArg.md#filepath)
- [isPython](RunScriptArg.md#ispython)
- [killOnMsg](RunScriptArg.md#killonmsg)
- [msgTo](RunScriptArg.md#msgto)
- [onMsg](RunScriptArg.md#onmsg)
- [returnJson](RunScriptArg.md#returnjson)
- [togetherWith](RunScriptArg.md#togetherwith)
- [useLog](RunScriptArg.md#uselog)

## Properties

### atDir

• `Optional` **atDir**: `string`

在哪个文件夹运行该文件(该文件夹会自动创建)

#### Defined in

src/node/run-script.ts:31

___

### copyFiles

• `Optional` **copyFiles**: `string`[]

一块拷贝到atDir目录下的文件或文件夹

#### Defined in

src/node/run-script.ts:48

___

### filePath

• **filePath**: `string`

脚本文件绝对路径，可以是.js或者.py

#### Defined in

src/node/run-script.ts:14

___

### isPython

• `Optional` **isPython**: `boolean`

该脚本是否为Python，默认自动判断

#### Defined in

src/node/run-script.ts:44

___

### killOnMsg

• `Optional` **killOnMsg**: `boolean`

是否在收到msg时就kill这个process；
如果returnJson为true时, 只有收到了nodejs-result时才会kill，
否则只要收到了任意一个msg就会kill

#### Defined in

src/node/run-script.ts:59

___

### msgTo

• `Optional` **msgTo**: `any`

向该脚本发送的信息,
在Python中 
  def receive():
     sys.stdin = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
     return json.loads(sys.stdin.read())
在Js中, process.on("message", (msgTo)=>{  })

#### Defined in

src/node/run-script.ts:40

___

### onMsg

• `Optional` **onMsg**: (`msg`: `any`) => `any`

#### Type declaration

▸ (`msg`): `any`

在收到脚本发来的msg之后运行的函数,
对于Python，如果returnJson为true, 该msg已经过JSON.parse

##### Parameters

| Name | Type |
| :------ | :------ |
| `msg` | `any` |

##### Returns

`any`

#### Defined in

src/node/run-script.ts:53

___

### returnJson

• `Optional` **returnJson**: `boolean`

是否对收到的msg进行JSON.parse，(仅用于Python)

#### Defined in

src/node/run-script.ts:63

___

### togetherWith

• `Optional` **togetherWith**: `Object`

一并拷贝过来的文件

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `files?` | `string`[] | 一并拷贝过来的文件的路径 |
| `relative?` | `boolean` | files的路径为相对还是绝对 |

#### Defined in

src/node/run-script.ts:18

___

### useLog

• `Optional` **useLog**: `boolean`

是否在end时Log

#### Defined in

src/node/run-script.ts:67
