[albert.gao](../README.md) / [Exports](../modules.md) / [PaddleOcr](../modules/PaddleOcr.md) / PosRaw

# Interface: PosRaw

[PaddleOcr](../modules/PaddleOcr.md).PosRaw

## Table of contents

### Properties

- [0](PaddleOcr.PosRaw.md#0)
- [1](PaddleOcr.PosRaw.md#1)

## Properties

### 0

• **0**: `number`

x

#### Defined in

src/node/run-script.ts:210

___

### 1

• **1**: `number`

y

#### Defined in

src/node/run-script.ts:212
