[albert.gao](../README.md) / [Exports](../modules.md) / XlsxWriterReturns

# Interface: XlsxWriterReturns

## Hierarchy

- [`MyWorker`](MyWorker.md)

  ↳ **`XlsxWriterReturns`**

## Table of contents

### Properties

- [kill](XlsxWriterReturns.md#kill)
- [output](XlsxWriterReturns.md#output)
- [terminate](XlsxWriterReturns.md#terminate)
- [worker](XlsxWriterReturns.md#worker)
- [write](XlsxWriterReturns.md#write)

## Properties

### kill

• **kill**: () => `void`

#### Type declaration

▸ (): `void`

停止该进程

##### Returns

`void`

#### Inherited from

[MyWorker](MyWorker.md).[kill](MyWorker.md#kill)

#### Defined in

src/workers/index.ts:36

___

### output

• **output**: [`MajorFunctionXlsxWriter`](../modules.md#majorfunctionxlsxwriter)

#### Defined in

src/workers/index.ts:138

___

### terminate

• **terminate**: () => `void`

#### Type declaration

▸ (): `void`

停止该进程

##### Returns

`void`

#### Inherited from

[MyWorker](MyWorker.md).[terminate](MyWorker.md#terminate)

#### Defined in

src/workers/index.ts:32

___

### worker

• **worker**: `ChildProcess`

返回worker实例

#### Inherited from

[MyWorker](MyWorker.md).[worker](MyWorker.md#worker)

#### Defined in

src/workers/index.ts:28

___

### write

• **write**: [`MajorFunctionXlsxWriter`](../modules.md#majorfunctionxlsxwriter)

#### Defined in

src/workers/index.ts:137
