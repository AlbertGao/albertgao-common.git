[albert.gao](../README.md) / [Exports](../modules.md) / [Excel](../modules/Excel.md) / Distribution

# Interface: Distribution

[Excel](../modules/Excel.md).Distribution

## Table of contents

### Properties

- [cell](Excel.Distribution.md#cell)
- [index](Excel.Distribution.md#index)
- [sheet](Excel.Distribution.md#sheet)
- [value](Excel.Distribution.md#value)
- [x](Excel.Distribution.md#x)
- [y](Excel.Distribution.md#y)

## Properties

### cell

• **cell**: `string`

cell位置

**`Example`**

```ts
"A2"; "AC4";
```

#### Defined in

src/common/index.ts:1410

___

### index

• **index**: `Object`

在源数组中的位置

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `x` | `number` | 列位置从0开始 |
| `y` | `number` | 行位置从0开始 |

#### Defined in

src/common/index.ts:1414

___

### sheet

• **sheet**: `string`

用于哪个sheet

#### Defined in

src/common/index.ts:1397

___

### value

• **value**: `any`

该位置的value

#### Defined in

src/common/index.ts:1401

___

### x

• **x**: `number`

x列的实际位置，从1开始

#### Defined in

src/common/index.ts:1393

___

### y

• **y**: `number`

y列的实际位置，从1开始

#### Defined in

src/common/index.ts:1405
