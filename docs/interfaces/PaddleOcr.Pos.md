[albert.gao](../README.md) / [Exports](../modules.md) / [PaddleOcr](../modules/PaddleOcr.md) / Pos

# Interface: Pos

[PaddleOcr](../modules/PaddleOcr.md).Pos

## Table of contents

### Properties

- [x](PaddleOcr.Pos.md#x)
- [y](PaddleOcr.Pos.md#y)

## Properties

### x

• **x**: `number`

#### Defined in

src/node/run-script.ts:226

___

### y

• **y**: `number`

#### Defined in

src/node/run-script.ts:227
