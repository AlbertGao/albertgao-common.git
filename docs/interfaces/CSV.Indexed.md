[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](../modules/CSV.md) / Indexed

# Interface: Indexed

[CSV](../modules/CSV.md).Indexed

第一层键为列名称，第二层键为非整数

## Indexable

▪ [key: `string`]: [`ColIndexed`](CSV.ColIndexed.md)
