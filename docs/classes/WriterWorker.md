[albert.gao](../README.md) / [Exports](../modules.md) / WriterWorker

# Class: WriterWorker

## Table of contents

### Constructors

- [constructor](WriterWorker.md#constructor)

### Properties

- [leastOccupied](WriterWorker.md#leastoccupied)
- [progressArr](WriterWorker.md#progressarr)
- [write](WriterWorker.md#write)

### Methods

- [kill](WriterWorker.md#kill)

## Constructors

### constructor

• **new WriterWorker**(`use`, `workerThreads?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `use` | ``"json"`` \| ``"xlsx"`` |
| `workerThreads` | `number` |

#### Defined in

src/workers/max-worker.ts:30

## Properties

### leastOccupied

• **leastOccupied**: `ComputedRef`<`number`\>

#### Defined in

src/workers/max-worker.ts:17

___

### progressArr

• **progressArr**: (`XlsxProgress` \| `JsonProgress`)[]

#### Defined in

src/workers/max-worker.ts:18

___

### write

• **write**: (`data`: `any`, `path`: `string`) => `Promise`<``true``\>

#### Type declaration

▸ (`data`, `path`): `Promise`<``true``\>

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `data` | `any` | 要写入的数据 |
| `path` | `string` | 路径 |

##### Returns

`Promise`<``true``\>

#### Defined in

src/workers/max-worker.ts:19

## Methods

### kill

▸ **kill**(): `void`

#### Returns

`void`

#### Defined in

src/workers/max-worker.ts:60
