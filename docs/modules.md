[albert.gao](README.md) / Exports

# albert.gao

## Table of contents

### Namespaces

- [CSV](modules/CSV.md)
- [Element](modules/Element.md)
- [Excel](modules/Excel.md)
- [Js](modules/Js.md)
- [MathExtra](modules/MathExtra.md)
- [PaddleOcr](modules/PaddleOcr.md)

### Enumerations

- [TIME](enums/TIME.md)

### Classes

- [WriterWorker](classes/WriterWorker.md)

### Interfaces

- [CsvAccFuncReturns](interfaces/CsvAccFuncReturns.md)
- [CsvInterpolateResult](interfaces/CsvInterpolateResult.md)
- [CsvToObjArrOptions](interfaces/CsvToObjArrOptions.md)
- [ExcelHandlerTools](interfaces/ExcelHandlerTools.md)
- [ExpressAppSetupFunc](interfaces/ExpressAppSetupFunc.md)
- [ExpressWssResult](interfaces/ExpressWssResult.md)
- [ExpressWssServerOptionsDeprecated](interfaces/ExpressWssServerOptionsDeprecated.md)
- [GetColorByPercentageOptions](interfaces/GetColorByPercentageOptions.md)
- [HandleWsClientFuncArg](interfaces/HandleWsClientFuncArg.md)
- [InjectCssOptions](interfaces/InjectCssOptions.md)
- [JsonAccReturns](interfaces/JsonAccReturns.md)
- [JsonWriterReturns](interfaces/JsonWriterReturns.md)
- [MyWorker](interfaces/MyWorker.md)
- [QueProgress](interfaces/QueProgress.md)
- [QueTask](interfaces/QueTask.md)
- [QueWatcher](interfaces/QueWatcher.md)
- [ReadAXlsxOptions](interfaces/ReadAXlsxOptions.md)
- [RunScriptArg](interfaces/RunScriptArg.md)
- [SecOf60sOptions](interfaces/SecOf60sOptions.md)
- [ServerInstance](interfaces/ServerInstance.md)
- [StartCountDownOptions](interfaces/StartCountDownOptions.md)
- [TitleTransform](interfaces/TitleTransform.md)
- [To60sFromSecOptions](interfaces/To60sFromSecOptions.md)
- [WsHandler](interfaces/WsHandler.md)
- [XlsxData](interfaces/XlsxData.md)
- [XlsxWriterReturns](interfaces/XlsxWriterReturns.md)

### Type Aliases

- [CalDistRawReturnType](modules.md#caldistrawreturntype)
- [CreateExpressWsOptions](modules.md#createexpresswsoptions)
- [CreateExpressWsResult](modules.md#createexpresswsresult)
- [ExcelTask](modules.md#exceltask)
- [GetSheet](modules.md#getsheet)
- [InjectScriptOptions](modules.md#injectscriptoptions)
- [JsonWriterMajorWrite](modules.md#jsonwritermajorwrite)
- [MajorFuncJsonAcc](modules.md#majorfuncjsonacc)
- [MajorFunctionXlsxWriter](modules.md#majorfunctionxlsxwriter)
- [ObjArrToCsvParseFunc](modules.md#objarrtocsvparsefunc)
- [Partial](modules.md#partial)
- [ServerSetupFunc](modules.md#serversetupfunc)
- [WriteData](modules.md#writedata)

### Variables

- [APP\_DIRNAME](modules.md#app_dirname)
- [IS\_IN\_ASAR](modules.md#is_in_asar)
- [PROJECT\_DIR](modules.md#project_dir)
- [\_\_projectDir](modules.md#__projectdir)
- [geoHuman](modules.md#geohuman)

### Functions

- [analyzeObject](modules.md#analyzeobject)
- [areTheSame](modules.md#arethesame)
- [arrayLike](modules.md#arraylike)
- [calDistRaw](modules.md#caldistraw)
- [clearScreen](modules.md#clearscreen)
- [clickOpenLink](modules.md#clickopenlink)
- [cpus](modules.md#cpus)
- [createExpressWs](modules.md#createexpressws)
- [createLocalCert](modules.md#createlocalcert)
- [csvAcc](modules.md#csvacc)
- [csvInterpolate](modules.md#csvinterpolate)
- [csvInterpolateSync](modules.md#csvinterpolatesync)
- [csvToObjArr](modules.md#csvtoobjarr)
- [csvToObjArr1](modules.md#csvtoobjarr1)
- [dateFormat](modules.md#dateformat)
- [dir](modules.md#dir)
- [downloadFile](modules.md#downloadfile)
- [e6bDA](modules.md#e6bda)
- [e6bGS](modules.md#e6bgs)
- [e6bGSbyHDG](modules.md#e6bgsbyhdg)
- [e6bHDG](modules.md#e6bhdg)
- [e6bHDGbyGS](modules.md#e6bhdgbygs)
- [e6bTAS](modules.md#e6btas)
- [e6bTRK](modules.md#e6btrk)
- [e6bWD](modules.md#e6bwd)
- [e6bWS](modules.md#e6bws)
- [exec](modules.md#exec)
- [exists](modules.md#exists)
- [expressWssServer](modules.md#expresswssserver)
- [fixArrayLikesDeep](modules.md#fixarraylikesdeep)
- [fixObjArrByTemplate](modules.md#fixobjarrbytemplate)
- [forceToDate](modules.md#forcetodate)
- [geoCrs](modules.md#geocrs)
- [geoDist](modules.md#geodist)
- [geoDistM](modules.md#geodistm)
- [geoFinalBearing](modules.md#geofinalbearing)
- [geoInitialBearing](modules.md#geoinitialbearing)
- [geoLatitude](modules.md#geolatitude)
- [geoLatitudeM](modules.md#geolatitudem)
- [geoLongitude](modules.md#geolongitude)
- [geoLongitudeM](modules.md#geolongitudem)
- [geoSmart](modules.md#geosmart)
- [get1stDayOfMonth](modules.md#get1stdayofmonth)
- [get1stDayOfNextMonth](modules.md#get1stdayofnextmonth)
- [getColorByPercentage](modules.md#getcolorbypercentage)
- [getCsvParams](modules.md#getcsvparams)
- [getFileSize](modules.md#getfilesize)
- [getImgBase64](modules.md#getimgbase64)
- [getInstance](modules.md#getinstance)
- [getIps](modules.md#getips)
- [getLastDayOfMonth](modules.md#getlastdayofmonth)
- [getModule](modules.md#getmodule)
- [getModules](modules.md#getmodules)
- [getMonthDifference](modules.md#getmonthdifference)
- [getMonthsBetween](modules.md#getmonthsbetween)
- [getOccupiedPorts](modules.md#getoccupiedports)
- [getUnOccupiedPort](modules.md#getunoccupiedport)
- [getUuid](modules.md#getuuid)
- [getWsAddress](modules.md#getwsaddress)
- [getWssClients](modules.md#getwssclients)
- [hToHm](modules.md#htohm)
- [hToHms](modules.md#htohms)
- [hToMs](modules.md#htoms)
- [handleExcel](modules.md#handleexcel)
- [handleWsClient](modules.md#handlewsclient)
- [in180s](modules.md#in180s)
- [in360s](modules.md#in360s)
- [injectCss](modules.md#injectcss)
- [injectScript](modules.md#injectscript)
- [isHTMLElement](modules.md#ishtmlelement)
- [isInElectronBrowser](modules.md#isinelectronbrowser)
- [isIterable](modules.md#isiterable)
- [isLocalhost](modules.md#islocalhost)
- [jsonAcc](modules.md#jsonacc)
- [jsonWriter](modules.md#jsonwriter)
- [keep0s](modules.md#keep0s)
- [killExcel](modules.md#killexcel)
- [latToHm](modules.md#lattohm)
- [latToHms](modules.md#lattohms)
- [lonToHm](modules.md#lontohm)
- [lonToHms](modules.md#lontohms)
- [mToHm](modules.md#mtohm)
- [mToHms](modules.md#mtohms)
- [mToMs](modules.md#mtoms)
- [objArrToCsv](modules.md#objarrtocsv)
- [objectAssignDeep](modules.md#objectassigndeep)
- [onPreload](modules.md#onpreload)
- [paddleOcr](modules.md#paddleocr)
- [queue](modules.md#queue)
- [readAXlsx](modules.md#readaxlsx)
- [replaceAll](modules.md#replaceall)
- [runScript](modules.md#runscript)
- [sToHm](modules.md#stohm)
- [sToHms](modules.md#stohms)
- [sToMs](modules.md#stoms)
- [secOf60s](modules.md#secof60s)
- [separate](modules.md#separate)
- [showUrl](modules.md#showurl)
- [sleep](modules.md#sleep)
- [splitArrByCol](modules.md#splitarrbycol)
- [splitArrByRow](modules.md#splitarrbyrow)
- [startCountDown](modules.md#startcountdown)
- [sumOf60s](modules.md#sumof60s)
- [to60sFromSec](modules.md#to60sfromsec)
- [writeABook](modules.md#writeabook)
- [writeDataToExcel](modules.md#writedatatoexcel)
- [wsOn](modules.md#wson)
- [wsSend](modules.md#wssend)
- [xlsxWriter](modules.md#xlsxwriter)

## Type Aliases

### CalDistRawReturnType

Ƭ **CalDistRawReturnType**: `Object`

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `distance` | `number` | 以米为单位的由点1指向点2的距离 |
| `finalBearing` | `number` | 以度数为单位的由点1指向点2的，在点2位置测量，取值范围(-180,180]。 |
| `initialBearing` | `number` | 以度数为单位的由点1指向点2的，在点1位置测量，取值范围(-180,180] |

#### Defined in

src/common/geo.ts:33

___

### CreateExpressWsOptions

Ƭ **CreateExpressWsOptions**: `Object`

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `CORS?` | `boolean` | 是否使用跨域，默认为true |
| `listen?` | ``true`` | 是否立即listen |
| `port?` | `number` | 初始指定一个端口， 默认值为8080， port之后后有可能会变更 |
| `portMode?` | ``"change-when-occupied"`` \| ``"kill-when-occupied"`` \| ``null`` | 端口模式 @"change-when-occupied" 如果端口被占用，自动分配一个新的端口 @"kill-when-occupied" 如果端口被占用，尝试kill掉该端口 @ null 默认值，不控制端口 |
| `setups?` | [`ExpressAppSetupFunc`](interfaces/ExpressAppSetupFunc.md)[] | 设置app.use系列 |

#### Defined in

src/node/network2.ts:22

___

### CreateExpressWsResult

Ƭ **CreateExpressWsResult**: `Object`

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `app` | `Express.Express` | Express的app |
| `port` | `number` | 经过变更后的port |
| `wss` | `Server` | WebSocket Server |

#### Defined in

src/node/network2.ts:8

___

### ExcelTask

Ƭ **ExcelTask**: (`tools`: [`ExcelHandlerTools`](interfaces/ExcelHandlerTools.md)) => `Promise`<`any` \| `void`\> \| `any` \| `void`

#### Type declaration

▸ (`tools`): `Promise`<`any` \| `void`\> \| `any` \| `void`

操作Excel的函数，待该函数执行完成会关闭Excel

##### Parameters

| Name | Type |
| :------ | :------ |
| `tools` | [`ExcelHandlerTools`](interfaces/ExcelHandlerTools.md) |

##### Returns

`Promise`<`any` \| `void`\> \| `any` \| `void`

#### Defined in

src/node/excel.ts:98

___

### GetSheet

Ƭ **GetSheet**: (`sheetName`: `string`) => `ExcelJs.Worksheet`

#### Type declaration

▸ (`sheetName`): `ExcelJs.Worksheet`

用于获取sheet的函数，如果无法获取，就创建并获取

##### Parameters

| Name | Type |
| :------ | :------ |
| `sheetName` | `string` |

##### Returns

`ExcelJs.Worksheet`

#### Defined in

src/node/excel.ts:53

___

### InjectScriptOptions

Ƭ **InjectScriptOptions**: `string` \| { `script`: `string`  } \| { `src`: `string`  } \| { `filePath`: `string`  }

#### Defined in

src/node/electron-utils.ts:77

___

### JsonWriterMajorWrite

Ƭ **JsonWriterMajorWrite**: (...`args`: [`string`, `any`] \| [`any`, string?]) => `Promise`<``true``\>

#### Type declaration

▸ (`...args`): `Promise`<``true``\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `...args` | [`string`, `any`] \| [`any`, string?] |

##### Returns

`Promise`<``true``\>

#### Defined in

src/workers/index.ts:83

___

### MajorFuncJsonAcc

Ƭ **MajorFuncJsonAcc**: (...`args`: [`string`, `JsonRows`] \| [`JsonRows`, string?]) => `Promise`<``true``\>

#### Type declaration

▸ (`...args`): `Promise`<``true``\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `...args` | [`string`, `JsonRows`] \| [`JsonRows`, string?] |

##### Returns

`Promise`<``true``\>

#### Defined in

src/workers/index.ts:182

___

### MajorFunctionXlsxWriter

Ƭ **MajorFunctionXlsxWriter**: (...`args`: [`string`, `any`] \| [`CsvLine`[], string?]) => `Promise`<``true``\>

#### Type declaration

▸ (`...args`): `Promise`<``true``\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `...args` | [`string`, `any`] \| [`CsvLine`[], string?] |

##### Returns

`Promise`<``true``\>

#### Defined in

src/workers/index.ts:133

___

### ObjArrToCsvParseFunc

Ƭ **ObjArrToCsvParseFunc**: (`value`: `any`, `key`: `string`, `index`: `number`) => `string`

#### Type declaration

▸ (`value`, `key`, `index`): `string`

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |
| `key` | `string` |
| `index` | `number` |

##### Returns

`string`

#### Defined in

src/common/index.ts:386

___

### Partial

Ƭ **Partial**<`T`\>: { [key in keyof T]?: T[key] }

#### Type parameters

| Name |
| :------ |
| `T` |

#### Defined in

src/common/index.ts:6

___

### ServerSetupFunc

Ƭ **ServerSetupFunc**: (`options`: [`ServerInstance`](interfaces/ServerInstance.md)) => `any`

#### Type declaration

▸ (`options`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`ServerInstance`](interfaces/ServerInstance.md) |

##### Returns

`any`

#### Defined in

src/node/network.ts:30

___

### WriteData

Ƭ **WriteData**: (`data`: [`ExcelData`](modules/Excel.md#exceldata), `pos?`: [`RawInputPos`](modules/Excel.md#rawinputpos), `parser?`: [`Parser`](modules/Excel.md#parser), `sheet?`: `string`) => `void`

#### Type declaration

▸ (`data`, `pos?`, `parser?`, `sheet?`): `void`

向Excel文档写入内容的函数

##### Parameters

| Name | Type |
| :------ | :------ |
| `data` | [`ExcelData`](modules/Excel.md#exceldata) |
| `pos?` | [`RawInputPos`](modules/Excel.md#rawinputpos) |
| `parser?` | [`Parser`](modules/Excel.md#parser) |
| `sheet?` | `string` |

##### Returns

`void`

#### Defined in

src/node/excel.ts:58

## Variables

### APP\_DIRNAME

• `Const` **APP\_DIRNAME**: `string`

如果electron未打包，则为工程目录，如果打包，则返回.exe所在目录

#### Defined in

src/node/electron-utils.ts:36

___

### IS\_IN\_ASAR

• `Const` **IS\_IN\_ASAR**: `boolean`

当前是否被electron-builder打包
app.asar.unpacked 或 在app.asar里

#### Defined in

src/node/electron-utils.ts:29

___

### PROJECT\_DIR

• `Const` **PROJECT\_DIR**: `string` = `__projectDir`

当albert.gao作为node_module时，工程文件根目录(node_modules的上一级文件夹)

#### Defined in

src/node/misc.ts:26

___

### \_\_projectDir

• `Const` **\_\_projectDir**: `string`

当albert.gao作为node_module时，工程文件根目录(node_modules的上一级文件夹)

#### Defined in

src/node/misc.ts:22

___

### geoHuman

• `Const` **geoHuman**: `Object`

可以给普通用户使用的经纬度、E6B计算简易化函数

#### Type declaration

| Name | Type |
| :------ | :------ |
| `e6bDA` | (`trk`: `number`, `tas`: `number`, `wd`: `number`, `ws`: `number`) => `number` |
| `e6bGS` | (`trk`: `number`, `tas`: `number`, `wd`: `number`, `ws`: `number`) => `number` |
| `e6bGSbyHDG` | (`hdg`: `number`, `tas`: `number`, `wd`: `number`, `ws`: `number`) => `number` |
| `e6bHDG` | (`trk`: `number`, `tas`: `number`, `wd`: `number`, `ws`: `number`) => `number` |
| `e6bHDGbyGS` | (`trk`: `number`, `gs`: `number`, `wd`: `number`, `ws`: `number`) => `number` |
| `e6bTAS` | (`trk`: `number`, `gs`: `number`, `wd`: `number`, `ws`: `number`) => `number` |
| `e6bTRK` | (`hdg`: `number`, `tas`: `number`, `wd`: `number`, `ws`: `number`) => `number` |
| `e6bWD` | (`trk`: `number`, `gs`: `number`, `hdg`: `number`, `tas`: `number`) => `number` |
| `e6bWS` | (`trk`: `number`, `gs`: `number`, `hdg`: `number`, `tas`: `number`) => `number` |
| `geoCrs` | (`lat1`: `number`, `lon1`: `number`, `lat2`: `number`, `lon2`: `number`) => `number` |
| `geoDist` | (`lat1`: `number`, `lon1`: `number`, `lat2`: `number`, `lon2`: `number`) => `number` |
| `geoDistM` | (`lat1`: `number`, `lon1`: `number`, `lat2`: `number`, `lon2`: `number`) => `number` |
| `geoFinalBearing` | (`lat1`: `number`, `lon1`: `number`, `lat2`: `number`, `lon2`: `number`) => `number` |
| `geoInitialBearing` | (`lat1`: `number`, `lon1`: `number`, `lat2`: `number`, `lon2`: `number`) => `number` |
| `geoLatitude` | (`lat1`: `number`, `lon1`: `number`, `brng`: `number`, `dist`: `number`) => `number` |
| `geoLatitudeM` | (`lat1`: `number`, `lon1`: `number`, `brng`: `number`, `dist`: `number`) => `number` |
| `geoLongitude` | (`lat1`: `number`, `lon1`: `number`, `brng`: `number`, `dist`: `number`) => `number` |
| `geoLongitudeM` | (`lat1`: `number`, `lon1`: `number`, `brng`: `number`, `dist`: `number`) => `number` |
| `geoSmart` | (`str`: `string` \| `number`, `decimals`: `number`) => `number` |
| `hToHm` | (`mode`: `number`, ...`args`: `number`[]) => `undefined` \| `string` |
| `hToHms` | (`mode`: `number`, ...`args`: `number`[]) => `undefined` \| `string` |
| `hToMs` | (...`args`: `number`[]) => `undefined` \| `string` |
| `in180s` | (...`args`: (`string` \| `number`)[]) => `number` |
| `in360s` | (...`args`: (`string` \| `number`)[]) => `number` |
| `latToHm` | (...`args`: `number`[]) => `undefined` \| `string` |
| `latToHms` | (...`args`: `number`[]) => `undefined` \| `string` |
| `lonToHm` | (...`args`: `number`[]) => `undefined` \| `string` |
| `lonToHms` | (...`args`: `number`[]) => `undefined` \| `string` |
| `mToHm` | (`mode`: `number`, ...`args`: `number`[]) => `undefined` \| `string` |
| `mToHms` | (`mode`: `number`, ...`args`: `number`[]) => `undefined` \| `string` |
| `mToMs` | (`mode`: `number`, ...`args`: `number`[]) => `undefined` \| `string` |
| `padding` | (`num`: `string` \| `number`, `frontLength`: `number`, `aftLength`: `number`) => `string` |
| `sToHm` | (`mode`: `number`, ...`args`: `number`[]) => `string` |
| `sToHms` | (`mode`: `number`, ...`args`: `number`[]) => `string` |
| `sToMs` | (`mode`: `number`, ...`args`: `number`[]) => `undefined` \| `string` |

#### Defined in

src/common/geo.ts:1024

## Functions

### analyzeObject

▸ **analyzeObject**(`origin`, `param1?`): [`Report`](interfaces/Js.Report.md)[]

获取一个Object、或Array的全部path，可用于lodash的has

**`Link`**

https://www.lodashjs.com/docs/lodash.has

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `origin` | [`Iterable`](modules/Js.md#iterable) | 要分析的对象 |
| `param1` | [`AnalyzeOptions`](interfaces/Js.AnalyzeOptions.md) | 选项 |

#### Returns

[`Report`](interfaces/Js.Report.md)[]

每个path和其值和类型

#### Defined in

src/common/index.ts:811

___

### areTheSame

▸ **areTheSame**(`obj0`, `obj1`, `options?`, `compares?`): `boolean`

计算两个字面量的每个路径值对应值是否相同

**`Example`**

```ts
let compare =  [
    {a: 1, b: 2, c: [{ d:"3", e:"4"}] },
    {b: 2, a: 1, c: [{ e:"4", d:"3"}] }
]
console.log(areTheSame(compare[0], compare[1])) // true
```

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `obj0` | [`Iterable`](modules/Js.md#iterable) | 第一个object |
| `obj1` | [`Iterable`](modules/Js.md#iterable) | 第二个object |
| `options` | [`AnalyzeOptions`](interfaces/Js.AnalyzeOptions.md) | 调用objectRootsDeep算法时的options |
| `compares` | [`CompareFunction`](modules/Js.md#comparefunction)[] | 默认用JSON.stringify来对比 对比结果如果为false则识别为不同 |

#### Returns

`boolean`

#### Defined in

src/common/index.ts:935

___

### arrayLike

▸ **arrayLike**<`T`\>(`source`, `strict?`): `any`[] \| ``false``

判断一个Object是否为Array Like。
Array和Set会被认为是Array Like。
如果Object是 { } 的被认为不是 ArrayLike。
如果Object有length、或\d的key，在strict时只要没有其他的key会被被认为是Array Like, 如果有的话，只有在不strict时被认为是ArrayLike。

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends [`Iterable`](modules/Js.md#iterable) |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `source` | `T` | `undefined` | 数据源，如果不是Object或者Array或者Set，会直接返回false |
| `strict` | `boolean` | `true` | (默认为true) 是否对Object启用严格模式。如果strict，必须所有的key都是 \d或者length。不strict时只要有1个键是\d或者length即认为是ArrayLike |

#### Returns

`any`[] \| ``false``

根据上述规则的值

#### Defined in

src/common/index.ts:985

___

### calDistRaw

▸ **calDistRaw**(`lat1`, `lon1`, `lat2`, `lon2`, `callback?`): [`CalDistRawReturnType`](modules.md#caldistrawreturntype)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `lat1` | `number` | 点1的纬度,单位是度数 |
| `lon1` | `number` | 点1的经度，单位是度数 |
| `lat2` | `number` | 点2的纬度,单位是度数 |
| `lon2` | `number` | 点2的经度,单位是度数 |
| `callback` | (`res`: [`CalDistRawReturnType`](modules.md#caldistrawreturntype)) => `any` | 传入函数以调用返回值 |

#### Returns

[`CalDistRawReturnType`](modules.md#caldistrawreturntype)

计算从(lat1.lon1)到(lat2,lon2)之间的参数, 以WGS-84坐标系精确计算,如果lat2或lat1相差超过180不能自动修正

#### Defined in

src/common/geo.ts:50

___

### clearScreen

▸ **clearScreen**(`isNode?`): `void`

清屏

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `isNode?` | `boolean` | 是否是node环境 |

#### Returns

`void`

#### Defined in

src/common/index.ts:2076

___

### clickOpenLink

▸ **clickOpenLink**(`href?`, `target?`): `void`

通过点击打开<a>标签打开链接

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `href` | `string` | `"/"` | 链接地址 |
| `target` | `string` | `"_blank"` | 打开方式 |

#### Returns

`void`

#### Defined in

src/browser/index.ts:286

___

### cpus

▸ **cpus**(): `number`

在nodejs或者js中返回Cpu数量

#### Returns

`number`

在nodejs或者js中返回Cpu数量

#### Defined in

src/workers/index.ts:16

___

### createExpressWs

▸ **createExpressWs**(`«destructured»`): `Promise`<[`CreateExpressWsResult`](modules.md#createexpresswsresult)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `«destructured»` | [`CreateExpressWsOptions`](modules.md#createexpresswsoptions) |

#### Returns

`Promise`<[`CreateExpressWsResult`](modules.md#createexpresswsresult)\>

#### Defined in

src/node/network2.ts:40

___

### createLocalCert

▸ **createLocalCert**(`«destructured»`): `Promise`<`HttpsOptions`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `«destructured»` | `CreateLocalCertOptions` |

#### Returns

`Promise`<`HttpsOptions`\>

#### Defined in

node_modules/albert.gao.mkcert/index.d.ts:13

___

### csvAcc

▸ **csvAcc**(`path`, `rows?`): [`CsvAccFuncReturns`](interfaces/CsvAccFuncReturns.md)

用子进程逐行写入csv文件

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` | 文件路径 |
| `rows?` | `CsvLine`[] | (可选)首次导入的数据 |

#### Returns

[`CsvAccFuncReturns`](interfaces/CsvAccFuncReturns.md)

进程对象

#### Defined in

src/workers/index.ts:56

___

### csvInterpolate

▸ **csvInterpolate**(`option`): `Promise`<[`CsvInterpolateResult`](interfaces/CsvInterpolateResult.md)\>

通过调用子进程对csv文件进行interpolation

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `option` | [`Option`](interfaces/CSV.GetCsvParams.Option.md) | 选项 |

#### Returns

`Promise`<[`CsvInterpolateResult`](interfaces/CsvInterpolateResult.md)\>

#### Defined in

src/node/csv-interpolate.ts:55

___

### csvInterpolateSync

▸ **csvInterpolateSync**(`option`): [`Row`](interfaces/CSV.FinalResult.Row.md)[]

读取.csv文件并转化为帧数据 (同步)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `option` | [`Option`](interfaces/CSV.CsvInterpolateSync.Option.md) | 设置 |

#### Returns

[`Row`](interfaces/CSV.FinalResult.Row.md)[]

读取结果

#### Defined in

src/common/index.ts:1832

___

### csvToObjArr

▸ **csvToObjArr**(`csv?`, `titleStart?`, `dataStart?`, `splitter?`, `titlesDictionary?`): `Object`[]

将csv字符串或者数组转换为Object组成的数组

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `csv` | `string` \| `string`[][] | `[]` | Csv数组或整个csv文档的字符串 |
| `titleStart` | `number` | `0` | 哪个index为列标题行 |
| `dataStart` | `number` | `undefined` | 从哪个index开始为数据 |
| `splitter` | `string` \| `RegExp` | `","` | 每列的分隔符，默认为", |
| `titlesDictionary` | [`TitleTransform`](interfaces/TitleTransform.md) | `{}` | 将原来的title变换到key的字典 |

#### Returns

`Object`[]

返回ObjectArr

#### Defined in

src/common/index.ts:455

___

### csvToObjArr1

▸ **csvToObjArr1**<`T`\>(`«destructured»?`): `T`[]

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends `Object` = `Object` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `«destructured»` | [`CsvToObjArrOptions`](interfaces/CsvToObjArrOptions.md) |

#### Returns

`T`[]

#### Defined in

src/common/index.ts:495

___

### dateFormat

▸ **dateFormat**(`fmt?`, `date?`): `string`

将日期整理为指定格式的字符串

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `fmt` | `string` | `"YY-mm-dd"` | 由Y(年), m(月), d(日), H(小时), M(分钟), S(秒) 和其他组成的字符串 |
| `date` | `string` \| `number` \| `Date` | `undefined` | 时间或时间对象 |

#### Returns

`string`

返回日期

#### Defined in

src/common/index.ts:318

___

### dir

▸ **dir**(`relativePath?`, `mode?`): `string`

如果在nodeJS或者electron开发环境下，返回相对于工程目录的的路径
如果electron已经打包，返回相对于.exe文件所在目录的路径

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `relativePath` | `string` \| `string`[] | `"database"` | 相对路径 |
| `mode?` | ``"Dir"`` \| ``"File"`` \| ``"dir"`` \| ``"file"`` | `undefined` | 路径模式 |

#### Returns

`string`

返回创建的路径

#### Defined in

src/node/electron-utils.ts:47

___

### downloadFile

▸ **downloadFile**(`url`, `path`, `config?`): `Promise`<`void`\>

下载文件的函数

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `url` | `string` | 下载的url，用于传给axios |
| `path` | `string` | 下载的文件路径 |
| `config` | `AxiosRequestConfig`<`any`\> | axios的配置 |

#### Returns

`Promise`<`void`\>

#### Defined in

src/node/electron-utils.ts:12

___

### e6bDA

▸ **e6bDA**(`trk`, `tas`, `wd`, `ws`): `number`

(所有速度单位只要一致即可)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `trk` | `number` | 航迹，以°为单位 |
| `tas` | `number` | 真空速 |
| `wd` | `number` | 风向，以°为单位 |
| `ws` | `number` | 风速 |

#### Returns

`number`

偏流角，以°为单位，右侧风时值为负数

#### Defined in

src/common/geo.ts:552

___

### e6bGS

▸ **e6bGS**(`trk`, `tas`, `wd`, `ws`): `number`

(所有速度单位只要一致即可)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `trk` | `number` | 航迹，以°为单位 |
| `tas` | `number` | 真空速 |
| `wd` | `number` | 风向，以°为单位 |
| `ws` | `number` | 风速 |

#### Returns

`number`

地速

#### Defined in

src/common/geo.ts:504

___

### e6bGSbyHDG

▸ **e6bGSbyHDG**(`hdg`, `tas`, `wd`, `ws`): `number`

(所有速度单位只要一致即可)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `hdg` | `number` | 航向，以°为单位 |
| `tas` | `number` | 真空速 |
| `wd` | `number` | 风向，以°为单位 |
| `ws` | `number` | 风速 |

#### Returns

`number`

地速

#### Defined in

src/common/geo.ts:525

___

### e6bHDG

▸ **e6bHDG**(`trk`, `tas`, `wd`, `ws`): `number`

(所有速度单位只要一致即可)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `trk` | `number` | 航迹，以°为单位 |
| `tas` | `number` | 真空速 |
| `wd` | `number` | 风向，以°为单位 |
| `ws` | `number` | 风速 |

#### Returns

`number`

航向，以°为单位

#### Defined in

src/common/geo.ts:577

___

### e6bHDGbyGS

▸ **e6bHDGbyGS**(`trk`, `gs`, `wd`, `ws`): `number`

(所有速度单位只要一致即可)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `trk` | `number` | 航迹，以°为单位 |
| `gs` | `number` | 地速 |
| `wd` | `number` | 风向，以°为单位 |
| `ws` | `number` | 风速 |

#### Returns

`number`

航向，以°为单位

#### Defined in

src/common/geo.ts:597

___

### e6bTAS

▸ **e6bTAS**(`trk`, `gs`, `wd`, `ws`): `number`

(所有速度单位只要一致即可)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `trk` | `number` | 航迹，以°为单位 |
| `gs` | `number` | 地速 |
| `wd` | `number` | 风向，以°为单位 |
| `ws` | `number` | 风速 |

#### Returns

`number`

真空速

#### Defined in

src/common/geo.ts:640

___

### e6bTRK

▸ **e6bTRK**(`hdg`, `tas`, `wd`, `ws`): `number`

(所有速度单位只要一致即可)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `hdg` | `number` | 航向，以°为单位 |
| `tas` | `number` | 真空速 |
| `wd` | `number` | 风向，以°为单位 |
| `ws` | `number` | 风速 |

#### Returns

`number`

航迹，以°为单位

#### Defined in

src/common/geo.ts:736

___

### e6bWD

▸ **e6bWD**(`trk`, `gs`, `hdg`, `tas`): `number`

(所有速度单位只要一致即可)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `trk` | `number` | 航迹，以°为单位 |
| `gs` | `number` | 地速 |
| `hdg` | `number` | 航向，以°为单位 |
| `tas` | `number` | 真空速 |

#### Returns

`number`

风向，以°为单位

#### Defined in

src/common/geo.ts:694

___

### e6bWS

▸ **e6bWS**(`trk`, `gs`, `hdg`, `tas`): `number`

(所有速度单位只要一致即可)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `trk` | `number` | 航迹，以°为单位 |
| `gs` | `number` | 地速 |
| `hdg` | `number` | 航向，以°为单位 |
| `tas` | `number` | 真空速 |

#### Returns

`number`

风速

#### Defined in

src/common/geo.ts:667

___

### exec

▸ **exec**(`command?`): `Promise`<`unknown`\>

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `command` | `string` | `"pip list"` |

#### Returns

`Promise`<`unknown`\>

#### Defined in

src/node/run-script.ts:194

___

### exists

▸ **exists**(`css`): `boolean`

页面上是否存在css选择器指定的元素

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `css` | `string` | css选择器 |

#### Returns

`boolean`

是否存在该元素

#### Defined in

src/browser/index.ts:40

___

### expressWssServer

▸ **expressWssServer**(`option`): `Promise`<[`ExpressWssResult`](interfaces/ExpressWssResult.md)\>

创建一个同时有express和webSocketServer的服务器

**`Deprecated`**

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `option` | [`ExpressWssServerOptionsDeprecated`](interfaces/ExpressWssServerOptionsDeprecated.md) | 服务器参数 |

#### Returns

`Promise`<[`ExpressWssResult`](interfaces/ExpressWssResult.md)\>

服务器实例

#### Defined in

src/node/network.ts:198

___

### fixArrayLikesDeep

▸ **fixArrayLikesDeep**<`T`\>(`origin`, `ignores?`): `T`

修改一个Object路径中所有含有key为数字或者length的对象为Array

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends [`Iterable`](modules/Js.md#iterable) |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `origin` | `T` | `undefined` | 数据源 |
| `ignores` | [`IgnoreFunc`](modules/Js.md#ignorefunc)[] | `[]` | 忽略的key |

#### Returns

`T`

#### Defined in

src/common/index.ts:1028

___

### fixObjArrByTemplate

▸ **fixObjArrByTemplate**<`T`\>(`rowTemplate`, `row`, `options?`, `onFix?`): `T`

根据template修复row中所有数据(deep)

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends `Object` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `rowTemplate` | `T` | row的模板 |
| `row` | `Object` | 需要修改的模板 |
| `options` | [`AnalyzeOptions`](interfaces/Js.AnalyzeOptions.md) | 同analyzeObject的options |
| `onFix` | [`WarnOptions`](interfaces/Js.WarnOptions.md) | 当出现改动时的options |

#### Returns

`T`

修复好的row，row的原值也会被更改

#### Defined in

src/common/index.ts:1071

___

### forceToDate

▸ **forceToDate**(`date?`, `defaultDate?`): `Date`

强制将量转换为Date对象

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `date?` | `any` | 需要转换的量 |
| `defaultDate` | `Date` | 默认值 |

#### Returns

`Date`

转换后的Date对象

#### Defined in

src/common/index.ts:2090

___

### geoCrs

▸ **geoCrs**(`lat1`, `lon1`, `lat2`, `lon2`): `number`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `lat1` | `number` | 点1的纬度,单位是度数 |
| `lon1` | `number` | 点1的经度，单位是度数 |
| `lat2` | `number` | 点2的纬度,单位是度数 |
| `lon2` | `number` | 点2的经度，单位是度数 |

#### Returns

`number`

计算点1至点2的方位，单位是度数，测量点位于点1

#### Defined in

src/common/geo.ts:306

___

### geoDist

▸ **geoDist**(`lat1`, `lon1`, `lat2`, `lon2`): `number`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `lat1` | `number` | 点1的纬度,单位是度数 |
| `lon1` | `number` | 点1的经度，单位是度数 |
| `lat2` | `number` | 点2的纬度,单位是度数 |
| `lon2` | `number` | 点2的经度，单位是度数 |

#### Returns

`number`

计算点1至点2的距离，单位是海里

#### Defined in

src/common/geo.ts:367

___

### geoDistM

▸ **geoDistM**(`lat1`, `lon1`, `lat2`, `lon2`): `number`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `lat1` | `number` | 点1的纬度,单位是度数 |
| `lon1` | `number` | 点1的经度，单位是度数 |
| `lat2` | `number` | 点2的纬度,单位是度数 |
| `lon2` | `number` | 点2的经度，单位是度数 |

#### Returns

`number`

计算点1至点2的距离，单位是米

#### Defined in

src/common/geo.ts:344

___

### geoFinalBearing

▸ **geoFinalBearing**(`lat1`, `lon1`, `lat2`, `lon2`): `number`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `lat1` | `number` | 点1的纬度,单位是度数 |
| `lon1` | `number` | 点1的经度，单位是度数 |
| `lat2` | `number` | 点2的纬度,单位是度数 |
| `lon2` | `number` | 点2的经度，单位是度数 |

#### Returns

`number`

计算点1至点2的方位，单位是度数，测量点位于点2

#### Defined in

src/common/geo.ts:325

___

### geoInitialBearing

▸ **geoInitialBearing**(`lat1`, `lon1`, `lat2`, `lon2`): `number`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `lat1` | `number` | 点1的纬度,单位是度数 |
| `lon1` | `number` | 点1的经度，单位是度数 |
| `lat2` | `number` | 点2的纬度,单位是度数 |
| `lon2` | `number` | 点2的经度，单位是度数 |

#### Returns

`number`

计算点1至点2的方位，单位是度数，测量点位于点1

#### Defined in

src/common/geo.ts:286

___

### geoLatitude

▸ **geoLatitude**(`lat1`, `lon1`, `brng`, `dist`): `number`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `lat1` | `number` | 点1的纬度,单位是度数 |
| `lon1` | `number` | 点1的经度，单位是度数 |
| `brng` | `number` | 点1至点2的方位，单位是度数 |
| `dist` | `number` | 点1至点2的距离，单位是海里 |

#### Returns

`number`

点2的纬度，单位是度数

#### Defined in

src/common/geo.ts:418

___

### geoLatitudeM

▸ **geoLatitudeM**(`lat1`, `lon1`, `brng`, `dist`): `number`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `lat1` | `number` | 点1的纬度,单位是度数 |
| `lon1` | `number` | 点1的经度，单位是度数 |
| `brng` | `number` | 点1至点2的方位，单位是度数 |
| `dist` | `number` | 点1至点2的距离，单位是米 |

#### Returns

`number`

点2的纬度，单位是度数

#### Defined in

src/common/geo.ts:386

___

### geoLongitude

▸ **geoLongitude**(`lat1`, `lon1`, `brng`, `dist`): `number`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `lat1` | `number` | 点1的纬度,单位是度数 |
| `lon1` | `number` | 点1的经度，单位是度数 |
| `brng` | `number` | 点1至点2的方位，单位是度数 |
| `dist` | `number` | 点1至点2的距离，单位是海里 |

#### Returns

`number`

点2的经度，单位是度数

#### Defined in

src/common/geo.ts:434

___

### geoLongitudeM

▸ **geoLongitudeM**(`lat1`, `lon1`, `brng`, `dist`): `number`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `lat1` | `number` | 点1的纬度,单位是度数 |
| `lon1` | `number` | 点1的经度，单位是度数 |
| `brng` | `number` | 点1至点2的方位，单位是度数 |
| `dist` | `number` | 点1至点2的距离，单位是米 |

#### Returns

`number`

点2的经度，单位是度数

#### Defined in

src/common/geo.ts:402

___

### geoSmart

▸ **geoSmart**(`str`, `decimals?`): `number`

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `str` | `string` \| `number` | `undefined` | 经或纬度字符串，不能同时输入两个 |
| `decimals` | `number` | `10` | 经纬度的小数保留位数，默认为10 |

#### Returns

`number`

转换后的经纬度

#### Defined in

src/common/geo.ts:448

___

### get1stDayOfMonth

▸ **get1stDayOfMonth**(`date?`): `Date`

计算date所在月的1号

#### Parameters

| Name | Type |
| :------ | :------ |
| `date` | `string` \| `number` \| `Date` |

#### Returns

`Date`

date所在月的1号

#### Defined in

src/common/index.ts:1183

___

### get1stDayOfNextMonth

▸ **get1stDayOfNextMonth**(`date?`): `Date`

计算date所在的下个月的1号

#### Parameters

| Name | Type |
| :------ | :------ |
| `date` | `string` \| `number` \| `Date` |

#### Returns

`Date`

date所在月的下一个月

#### Defined in

src/common/index.ts:1194

___

### getColorByPercentage

▸ **getColorByPercentage**(`percentage`, `options?`): `string`

根据百分比线性计算颜色

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `percentage` | `number` | 百分比0-100之间 |
| `options` | [`GetColorByPercentageOptions`](interfaces/GetColorByPercentageOptions.md) | 颜色计算参数 |

#### Returns

`string`

#### Defined in

src/common/index.ts:296

___

### getCsvParams

▸ **getCsvParams**(`options`): [`Result`](interfaces/CSV.GetCsvParams.Result.md)

初步读取csv文件参数和数据

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `options` | [`Option`](interfaces/CSV.GetCsvParams.Option.md) | 读取设置 |

#### Returns

[`Result`](interfaces/CSV.GetCsvParams.Result.md)

读取结果

#### Defined in

src/common/index.ts:1710

___

### getFileSize

▸ **getFileSize**(`path`, `digits?`): `string`

获取文件大小

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `path` | `string` | `undefined` | 文件完整路径 |
| `digits` | `number` | `1` | - |

#### Returns

`string`

**MB **GB 或 **KB或**b格式

#### Defined in

src/node/misc.ts:151

___

### getImgBase64

▸ **getImgBase64**(`img`): `Object`

将html img元素转换为base64

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `img` | `HTMLImageElement` | 图片的HTML元素 |

#### Returns

`Object`

该图片的base64内容

| Name | Type |
| :------ | :------ |
| `base64` | `string` |
| `data` | `string` |
| `ext` | `string` |

#### Defined in

src/browser/index.ts:10

___

### getInstance

▸ **getInstance**(`obj`): [`Types`](modules/Js.md#types)

使用Object.prototype.toString.call测试字面量类型

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `obj` | `any` | 要测试的 |

#### Returns

[`Types`](modules/Js.md#types)

字面量类型

#### Defined in

src/common/index.ts:24

___

### getIps

▸ **getIps**(): `string`[]

#### Returns

`string`[]

#### Defined in

node_modules/albert.gao.mkcert/index.d.ts:2

___

### getLastDayOfMonth

▸ **getLastDayOfMonth**(`date?`): `Date`

计算当前月的最后一天

#### Parameters

| Name | Type |
| :------ | :------ |
| `date` | `string` \| `number` \| `Date` |

#### Returns

`Date`

date所在月的最后一天

#### Defined in

src/common/index.ts:1218

___

### getModule

▸ **getModule**(`path`): `any`

require某个js

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` | js文件的完整路径 |

#### Returns

`any`

返回require该js文件

#### Defined in

src/node/misc.ts:100

___

### getModules

▸ **getModules**(`directory`, `filter?`): { `module`: `any` ; `path`: `string`  }[]

require某个文件夹下的所有js, 如果该模块导出的内容有.priority，则该数值越大，越优先导入，否则按照文件名顺序导入

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `directory` | `string` | 文件夹完整路径路径 |
| `filter` | (`path`: `string`) => `boolean` | 过滤器, 返回true则加入 |

#### Returns

{ `module`: `any` ; `path`: `string`  }[]

该文件夹下所有require的js

#### Defined in

src/node/misc.ts:115

___

### getMonthDifference

▸ **getMonthDifference**(`a`, `b`, `useDigit?`): `number`

计算两个时间的月数差值

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `a` | `string` \| `number` \| `Date` | `undefined` | 被减数 |
| `b` | `string` \| `number` \| `Date` | `undefined` | 减数 |
| `useDigit` | `boolean` | `false` | 如果为是，则计算方法为每12个月365.25天，否则为月份差整数值 |

#### Returns

`number`

月份差值

#### Defined in

src/common/index.ts:354

___

### getMonthsBetween

▸ **getMonthsBetween**(`date0`, `date1`): `Date`[]

计算两个date之间的所有月的1号的Date组成的数组

#### Parameters

| Name | Type |
| :------ | :------ |
| `date0` | `string` \| `number` \| `Date` |
| `date1` | `string` \| `number` \| `Date` |

#### Returns

`Date`[]

获得两个date之间的所有months数组

#### Defined in

src/common/index.ts:1230

___

### getOccupiedPorts

▸ **getOccupiedPorts**(): `Promise`<`number`[]\>

获取已被占用的端口

#### Returns

`Promise`<`number`[]\>

已被占用的端口

#### Defined in

src/node/network.ts:151

___

### getUnOccupiedPort

▸ **getUnOccupiedPort**(`port`, `...other`): `Promise`<`number`\>

根据需要的端口号是否被占用按需获取一个新的端口号

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `port` | `string` \| `number` | 需要的端口号 |
| `...other` | `number`[] \| `string`[] | 其他不可用的端口号 |

#### Returns

`Promise`<`number`\>

获取的新的端口号

#### Defined in

src/node/network.ts:179

___

### getUuid

▸ **getUuid**(`before?`): `string`

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `before` | `string` | `"abc-"` | 前缀 |

#### Returns

`string`

随机不重复/极小概率重复码

#### Defined in

src/common/index.ts:15

___

### getWsAddress

▸ **getWsAddress**(`route?`): `string`

根据window.location获取ws://或者wss://开头的url

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `route` | ``""`` \| \`/${string}\` | `""` | 后面的路径 |

#### Returns

`string`

类似于wss://localhost:8080的地址

#### Defined in

src/browser/index.ts:240

___

### getWssClients

▸ **getWssClients**(`wssArr`, `onlineOnly?`): `WebSocket.WebSocket`[]

获取WebSocketServer的所有用户

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `wssArr` | `Server`<`WebSocket`\>[] | `undefined` | WebSocketServer列表 |
| `onlineOnly` | `boolean` | `true` | 是否只筛选在线client |

#### Returns

`WebSocket.WebSocket`[]

返回clients数组

#### Defined in

src/node/network.ts:309

___

### hToHm

▸ **hToHm**(`mode?`, `...args`): `undefined` \| `string`

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `mode` | `number` | `0` | 模式: 默认为0; 其绝对值为保留小数点后几位; 如果是大于等于0，则使用 :分隔，否则使用 °'" 分隔 |
| `...args` | `number`[] | `undefined` | 小时数 / 度数 |

#### Returns

`undefined` \| `string`

计算出经纬度或者时间，以字符串形式返回 "度分"

#### Defined in

src/common/geo.ts:927

___

### hToHms

▸ **hToHms**(`mode?`, `...args`): `undefined` \| `string`

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `mode` | `number` | `0` | 模式: 默认为0; 其绝对值为保留小数点后几位; 如果是大于等于0，则使用 :分隔，否则使用 °'" 分隔 |
| `...args` | `number`[] | `undefined` | 小时数 / 度数 |

#### Returns

`undefined` \| `string`

计算出经纬度或者时间，以字符串形式返回 "度分秒"

#### Defined in

src/common/geo.ts:912

___

### hToMs

▸ **hToMs**(`...args`): `undefined` \| `string`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `...args` | `number`[] | 小时数 / 度数 |

#### Returns

`undefined` \| `string`

计算出经纬度或者时间，以字符串形式返回 "分秒"，小数点后保留1位

#### Defined in

src/common/geo.ts:941

___

### handleExcel

▸ **handleExcel**(`fileName`, `task?`, `saveTo?`): `Promise`<`void`\>

使用ExcelJs读写Excel数据

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `fileName` | `string` | `undefined` | 文件位置 |
| `task` | [`ExcelTask`](modules.md#exceltask) | `undefined` | 在excel关闭之前需要做的任务 |
| `saveTo` | `string` | `fileName` | 保存文件位置，默认 和 filename 相同 |

#### Returns

`Promise`<`void`\>

#### Defined in

src/node/excel.ts:111

___

### handleWsClient

▸ **handleWsClient**(`wssArr?`, `broadcast?`, `handlers?`): `void`

批量处理webSocketServer的函数

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `wssArr` | `Server`<`WebSocket`\>[] | `[]` | WebSocketServer数组 |
| `broadcast` | `Function` | `undefined` | 广播函数 |
| `handlers` | [`WsHandler`](interfaces/WsHandler.md)[] | `[]` | 处理函数 |

#### Returns

`void`

#### Defined in

src/node/network.ts:330

___

### in180s

▸ **in180s**(`...args`): `number`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `...args` | (`string` \| `number`)[] | 度数 |

#### Returns

`number`

将所有度数加到一起之后，取值至(-180,180]

#### Defined in

src/common/geo.ts:774

___

### in360s

▸ **in360s**(`...args`): `number`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `...args` | (`string` \| `number`)[] | 度数 |

#### Returns

`number`

将所有度数加到一起之后，取值至[0,360)

#### Defined in

src/common/geo.ts:784

___

### injectCss

▸ **injectCss**(`option`): `void`

向webContents注入css

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `option` | [`InjectCssOptions`](interfaces/InjectCssOptions.md) | 向webContents注入的css选项 |

#### Returns

`void`

#### Defined in

src/node/electron-utils.ts:142

___

### injectScript

▸ **injectScript**(`injection`): `void`

向webContents注入script

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `injection` | [`InjectScriptOptions`](modules.md#injectscriptoptions) | 向webContents注入的内容 |

#### Returns

`void`

#### Defined in

src/node/electron-utils.ts:102

___

### isHTMLElement

▸ **isHTMLElement**(`value?`): `boolean`

字面量是否为html元素

#### Parameters

| Name | Type |
| :------ | :------ |
| `value?` | `any` |

#### Returns

`boolean`

#### Defined in

node_modules/@types/lodash/common/lang.d.ts:549

___

### isInElectronBrowser

▸ **isInElectronBrowser**(): `boolean`

**`Deprecated`**

是否在已启用了electron Remote的electron-browser-window里
已弃用，可以直接复制本段源码使用

#### Returns

`boolean`

是否在已启用了electron Remote的electron-browser-window里

#### Defined in

src/browser/index.ts:306

___

### isIterable

▸ **isIterable**(`obj`): `boolean`

判断一个元素是否为Array Object Set

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `obj` | `any` | 任意字面量 |

#### Returns

`boolean`

是否

#### Defined in

src/common/index.ts:800

___

### isLocalhost

▸ **isLocalhost**(`url`): `boolean`

判断https?开头的url是否属于本地ip

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `url` | `string` | url |

#### Returns

`boolean`

是否属于本地ip

#### Defined in

src/node/network.ts:135

___

### jsonAcc

▸ **jsonAcc**(`path?`, `rows?`): [`JsonAccReturns`](interfaces/JsonAccReturns.md)

用子进程追加json文件

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path?` | `string` | 要导出的json文件路径 |
| `rows?` | `JsonRows` | 首次要导入的数据 |

#### Returns

[`JsonAccReturns`](interfaces/JsonAccReturns.md)

返回{worker,kill,terminate,assign}

#### Defined in

src/workers/index.ts:197

___

### jsonWriter

▸ **jsonWriter**(`path?`, `obj?`): [`JsonWriterReturns`](interfaces/JsonWriterReturns.md)

用子进程写入json文件

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path?` | `string` | 路径 |
| `obj?` | `any` | (可选)可以转为json的内容 |

#### Returns

[`JsonWriterReturns`](interfaces/JsonWriterReturns.md)

{worker,kill,terminate,write}

#### Defined in

src/workers/index.ts:96

___

### keep0s

▸ **keep0s**(`num?`, `frontLength`, `aftLength?`): `string`

保留数字前后的"0"

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `num` | `string` \| `number` | `""` | 传入的数字 |
| `frontLength` | `number` | `undefined` | 小数点前保留多少个0，如果数字位数大于frontLength，则不考虑 |
| `aftLength` | `number` | `0` | 小数点后保留多少个0，如果小数点后精度已高于aftLenth，则不考虑 |

#### Returns

`string`

整理后的数字

#### Defined in

src/common/index.ts:72

___

### killExcel

▸ **killExcel**(): `void`

强制关闭Excel应用

#### Returns

`void`

#### Defined in

src/node/misc.ts:31

___

### latToHm

▸ **latToHm**(`...args`): `undefined` \| `string`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `...args` | `number`[] | 纬度 |

#### Returns

`undefined` \| `string`

N/S 度°分'，小数点后保留1位

#### Defined in

src/common/geo.ts:955

___

### latToHms

▸ **latToHms**(`...args`): `undefined` \| `string`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `...args` | `number`[] | 纬度 |

#### Returns

`undefined` \| `string`

N/S 度°分'秒"，小数点后保留1位

#### Defined in

src/common/geo.ts:971

___

### lonToHm

▸ **lonToHm**(`...args`): `undefined` \| `string`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `...args` | `number`[] | 经度 |

#### Returns

`undefined` \| `string`

W/E 度°分'，小数点后保留1位

#### Defined in

src/common/geo.ts:987

___

### lonToHms

▸ **lonToHms**(`...args`): `undefined` \| `string`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `...args` | `number`[] | 经度 |

#### Returns

`undefined` \| `string`

W/E 度°分'秒"，小数点后保留1位

#### Defined in

src/common/geo.ts:1006

___

### mToHm

▸ **mToHm**(`mode?`, `...args`): `undefined` \| `string`

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `mode` | `number` | `0` | 模式: 默认为0; 其绝对值为保留小数点后几位; 如果是大于等于0，则使用 :分隔，否则使用 °'" 分隔 |
| `...args` | `number`[] | `undefined` | 分钟数 |

#### Returns

`undefined` \| `string`

计算出经纬度或者时间，以字符串形式返回 "度分"

#### Defined in

src/common/geo.ts:878

___

### mToHms

▸ **mToHms**(`mode?`, `...args`): `undefined` \| `string`

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `mode` | `number` | `0` | 模式: 默认为0; 其绝对值为保留小数点后几位; 如果是大于等于0，则使用 :分隔，否则使用 °'" 分隔 |
| `...args` | `number`[] | `undefined` | 分钟数 |

#### Returns

`undefined` \| `string`

计算出经纬度或者时间，以字符串形式返回 "度分秒"

#### Defined in

src/common/geo.ts:863

___

### mToMs

▸ **mToMs**(`mode?`, `...args`): `undefined` \| `string`

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `mode` | `number` | `0` | 模式: 默认为0; 其绝对值为保留小数点后几位; 如果是大于等于0，则使用 :分隔，否则使用 °'" 分隔 |
| `...args` | `number`[] | `undefined` | 分钟数 |

#### Returns

`undefined` \| `string`

计算出经纬度或者时间，以字符串形式返回 "分秒"

#### Defined in

src/common/geo.ts:897

___

### objArrToCsv

▸ **objArrToCsv**<`T`\>(`rows?`, `titlesFrom?`, `parser?`, `withTitles?`, `titlesDictionary?`): `string`[][]

将由Object组成的数组转换为数组组成的数组的Array

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends `Object` |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `rows` | `T`[] | `[]` | 数据源 |
| `titlesFrom` | `number` | `0` | 从第几行获取title （从0开始） |
| `parser` | [`ObjArrToCsvParseFunc`](modules.md#objarrtocsvparsefunc) | `undefined` | 格式转换器 |
| `withTitles` | `boolean` | `true` | 是否需要加上第一行titles |
| `titlesDictionary` | [`TitleTransform`](interfaces/TitleTransform.md) | `{}` | titles的转换字典，数据会根据这个字典key的顺序来对"列"进行排序 |

#### Returns

`string`[][]

返回csv格式的数组

#### Defined in

src/common/index.ts:409

___

### objectAssignDeep

▸ **objectAssignDeep**<`T`, `U`\>(`target`, `source`, `cloneTarget?`): `T` & `U`

**`Description`**

Object.assign 的改进版，深度赋值

**`Example`**

```ts
let a = { a: { a:"1" } };
let b = { a: { b:"1" } };
objectAssignDeep(a, b);
console.log(a) // { a: { a: '1', b: '2' } }
```

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends `Object` |
| `U` | extends `Object` |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `target` | `T` | `undefined` | Object.assign的第1个参数 |
| `source` | `U` | `undefined` | Object.assign的第2个参数 |
| `cloneTarget` | `boolean` | `false` | 是否clone target以防止修改target本身 |

#### Returns

`T` & `U`

修改后的target

#### Defined in

src/common/index.ts:1165

___

### onPreload

▸ **onPreload**(`cb`): `Promise`<``true``\>

等待DOMContentLoaded事件

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `cb` | () => `any` | 在DOMContentLoaded之后运行的函数 |

#### Returns

`Promise`<``true``\>

在DOMContentLoaded之后返回Promise<true>

#### Defined in

src/browser/index.ts:320

___

### paddleOcr

▸ **paddleOcr**(`imgPath`, `separator?`): `Promise`<[`OcrResult`](interfaces/PaddleOcr.OcrResult.md)\>

通过直接调用python脚本和paddleOcr来实现OCR文字识别

**`Requires`**

需要安装Anaconda和paddleOCR

**`Link`**

Anaconda https://www.anaconda.com/

**`Link`**

paddleocr https://pypi.org/project/paddleocr/

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `imgPath` | `string` | `undefined` | img文件的绝对路径 |
| `separator` | `string` | `"\n"` | 输出text时的分隔符 |

#### Returns

`Promise`<[`OcrResult`](interfaces/PaddleOcr.OcrResult.md)\>

返回OCR文字识别结果

#### Defined in

src/node/run-script.ts:257

___

### queue

▸ **queue**<`Result`\>(`tasks?`, `watcher?`, `maxChunk?`): `Promise`<`Result`[]\>

根据最大同时进行任务的数量，创建一个任务队列

#### Type parameters

| Name |
| :------ |
| `Result` |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `tasks` | [`QueTask`](interfaces/QueTask.md)<`Result`\>[] | `[]` | 任务队列 |
| `watcher?` | [`QueWatcher`](interfaces/QueWatcher.md) | `console.log` | 回调{waiting, finished, processing, total} |
| `maxChunk?` | `number` | `undefined` | 队列最大长度，默认为cpu数量 |

#### Returns

`Promise`<`Result`[]\>

返回运行结果数组

#### Defined in

src/workers/index.ts:271

___

### readAXlsx

▸ **readAXlsx**<`Row`\>(`«destructured»`): `Row`[]

#### Type parameters

| Name | Type |
| :------ | :------ |
| `Row` | extends `Object` = `Object` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `«destructured»` | [`ReadAXlsxOptions`](interfaces/ReadAXlsxOptions.md) |

#### Returns

`Row`[]

#### Defined in

src/node/misc.ts:82

___

### replaceAll

▸ **replaceAll**(`src`, `find`, `replace?`, `raw?`): `string`

字符串全部替换

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `src` | `string` | `undefined` | 原字符串 |
| `find` | `string` | `undefined` | 查找字符串 |
| `replace` | `string` | `""` | 替换字符串 |
| `raw` | `boolean` | `true` | 是否在构建正则表达式是把find需要转义的字符进行转义 |

#### Returns

`string`

替换后的字符串

#### Defined in

src/common/index.ts:2031

___

### runScript

▸ **runScript**(`option`): `Promise`<`any`\>

运行一个nodejs或python脚本

**`Example`**

```ts
// 在python中接收msgTo
 // def receive():
 //    import sys
 //    import io
 //    import json
 //    sys.stdin = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
 //    return json.loads(sys.stdin.read())
```

**`Example`**

```ts
// 在python中将结果返回给nodejs
 // def returns(result):
 //    import json
 //    print(f"<nodejs-result>{json.dumps(result)}</nodejs-result>")
```

#### Parameters

| Name | Type |
| :------ | :------ |
| `option` | [`RunScriptArg`](interfaces/RunScriptArg.md) |

#### Returns

`Promise`<`any`\>

该脚本的运行结果

#### Defined in

src/node/run-script.ts:85

___

### sToHm

▸ **sToHm**(`mode?`, `...args`): `string`

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `mode` | `number` | `0` | 模式: 默认为0; 其绝对值为保留小数点后几位; 如果是大于等于0，则使用 :分隔，否则使用 °'" 分隔 |
| `...args` | `number`[] | `undefined` | 秒数 |

#### Returns

`string`

计算出经纬度或者时间，以字符串形式返回 "度分"

#### Defined in

src/common/geo.ts:817

___

### sToHms

▸ **sToHms**(`mode?`, `...args`): `string`

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `mode` | `number` | `0` | 模式: 默认为0; 其绝对值为保留小数点后几位; 如果是大于等于0，则使用 :分隔，否则使用 °'" 分隔 |
| `...args` | `number`[] | `undefined` | 秒数 |

#### Returns

`string`

计算出经纬度或者时间，以字符串形式返回 "度分秒"

#### Defined in

src/common/geo.ts:795

___

### sToMs

▸ **sToMs**(`mode?`, `...args`): `undefined` \| `string`

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `mode` | `number` | `0` | 模式: 默认为0; 其绝对值为保留小数点后几位; 如果是大于等于0，则使用 :分隔，否则使用 °'" 分隔 |
| `...args` | `number`[] | `undefined` | 秒数 |

#### Returns

`undefined` \| `string`

计算出经纬度或者时间，以字符串形式返回 "分秒"

#### Defined in

src/common/geo.ts:838

___

### secOf60s

▸ **secOf60s**(`options?`, `...timeStrings`): `number`

将60进制字符串转化成秒

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `options?` | `string` \| [`SecOf60sOptions`](interfaces/SecOf60sOptions.md) | 第一个参数可以使选项，也可以是时间字符串 |
| `...timeStrings` | `string`[] | 60进制时间字符串 |

#### Returns

`number`

总秒数

#### Defined in

src/common/index.ts:127

___

### separate

▸ **separate**(`str`, `reg`, `allowEmpty?`): `string`[]

根据正则表达式将字符串分割为按顺序组成的匹配、不匹配正则表达式的字符串的数组

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `str` | `string` | `undefined` | 字符串 |
| `reg` | `string` \| `RegExp` | `undefined` | 匹配的正则 |
| `allowEmpty` | `boolean` | `true` | 是否允许空字符串 |

#### Returns

`string`[]

返回匹配的结果

#### Defined in

src/common/index.ts:535

___

### showUrl

▸ **showUrl**(): `void`

在页面底部显示url

#### Returns

`void`

#### Defined in

src/browser/index.ts:48

___

### sleep

▸ **sleep**(`interval`): `Promise`<`void`\>

等待一个毫秒数

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `interval` | `number` | 毫秒数 |

#### Returns

`Promise`<`void`\>

#### Defined in

src/common/index.ts:61

___

### splitArrByCol

▸ **splitArrByCol**<`T`\>(`arr?`, `cols?`): `T`[][]

根据份数进行数组分割

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `arr` | `T`[] | `[]` | 要分割的数组 |
| `cols` | `undefined` \| `number` | `3` | 要分割的份数 |

#### Returns

`T`[][]

分割后的数组

#### Defined in

src/common/index.ts:48

___

### splitArrByRow

▸ **splitArrByRow**<`T`\>(`arr?`, `maxLen?`): `T`[][]

根据最大length进行数组分割

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `arr` | `T`[] | `[]` | 要分割的数组 |
| `maxLen` | `undefined` \| `number` | `5` | 最大length |

#### Returns

`T`[][]

分割后的数组

#### Defined in

src/common/index.ts:35

___

### startCountDown

▸ **startCountDown**(`option`): `Object`

创建一个倒计时窗口

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `option` | [`StartCountDownOptions`](interfaces/StartCountDownOptions.md) | 倒计时参数 |

#### Returns

`Object`

倒计时窗口的实例

| Name | Type |
| :------ | :------ |
| `msg` | `MessageHandler` |
| `promise` | `Promise`<``1``\> |

#### Defined in

src/browser/index.ts:141

___

### sumOf60s

▸ **sumOf60s**(`options?`, `...args`): `string`

计算60进制字符串的总和

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `options?` | `string` \| `number` \| [`To60sFromSecOptions`](interfaces/To60sFromSecOptions.md) | 选项，可以不填 |
| `...args` | `number`[] \| `string`[] | 60进制数字或字符串数组 |

#### Returns

`string`

返回计算后的数值

#### Defined in

src/common/index.ts:232

___

### to60sFromSec

▸ **to60sFromSec**(`options?`, `...args`): `string`

从Number计算出60进制字符串

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `options?` | `number` \| [`To60sFromSecOptions`](interfaces/To60sFromSecOptions.md) | 选项，可以不填 |
| `...args` | `number`[] | 数字 |

#### Returns

`string`

计算出的60进制字符串

#### Defined in

src/common/index.ts:181

___

### writeABook

▸ **writeABook**(`path`, `data`, `titlesFrom?`, `parser?`): `void`

自动将数据写入excel

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `path` | `string` | `undefined` | - |
| `data` | `Object`[] \| `any`[][] | `undefined` | * |
| `titlesFrom?` | `number` | `0` | 从哪个index读取titles |
| `parser?` | [`ObjArrToCsvParseFunc`](modules.md#objarrtocsvparsefunc) | `undefined` | - |

#### Returns

`void`

#### Defined in

src/node/misc.ts:43

___

### writeDataToExcel

▸ **writeDataToExcel**(`path`, `sheetName`, `data`): `Promise`<`unknown`\>

**`Description`**

调用python将数据写入到Excel
需要 python 3.x 和 xlwings

**`Deprecated`**

已弃用

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` | Excel的文件路径 |
| `sheetName` | `string` | sheet的名称 |
| `data` | [`XlsxData`](interfaces/XlsxData.md)[] | 要写入的数据 |

#### Returns

`Promise`<`unknown`\>

#### Defined in

src/node/excel.ts:26

___

### wsOn

▸ **wsOn**(`handlers?`, `ws?`): `WebSocket`

批量处理ws.onmessage事件

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `handlers` | (`data`: `any`) => `any`[] | `[]` | 收到msg之后的函数, 其中msg已经JSON.parse过了 |
| `ws` | `WebSocket` | `undefined` | WebSocket实例 |

#### Returns

`WebSocket`

#### Defined in

src/browser/index.ts:251

___

### wsSend

▸ **wsSend**(`msg`, `ws`): `void`

利用ws发送JSON数据

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `msg` | `any` | 要用ws发送的数据 |
| `ws` | `WebSocket` | WebSocket实例 |

#### Returns

`void`

#### Defined in

src/browser/index.ts:277

___

### xlsxWriter

▸ **xlsxWriter**(`path?`, `data?`): [`XlsxWriterReturns`](interfaces/XlsxWriterReturns.md)

用子进程写入xlsx文件

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path?` | `string` | 路径 |
| `data?` | `CsvLine`[] | * |

#### Returns

[`XlsxWriterReturns`](interfaces/XlsxWriterReturns.md)

{worker,kill,terminate,write}

#### Defined in

src/workers/index.ts:146
