[albert.gao](../README.md) / [Exports](../modules.md) / Js

# Namespace: Js

## Table of contents

### Interfaces

- [AnalyzeOptions](../interfaces/Js.AnalyzeOptions.md)
- [ObjectRouteReport](../interfaces/Js.ObjectRouteReport.md)
- [Report](../interfaces/Js.Report.md)
- [WarnOptions](../interfaces/Js.WarnOptions.md)

### Type Aliases

- [CompareFunction](Js.md#comparefunction)
- [IgnoreFunc](Js.md#ignorefunc)
- [Iterable](Js.md#iterable)
- [Route](Js.md#route)
- [StopElement](Js.md#stopelement)
- [StopPositionEle](Js.md#stoppositionele)
- [Types](Js.md#types)

### Functions

- [defaultCompareFunction](Js.md#defaultcomparefunction)
- [hasArrBefore](Js.md#hasarrbefore)
- [toRoutePath](Js.md#toroutepath)

## Type Aliases

### CompareFunction

Ƭ **CompareFunction**: (`a`: `any`, `b`: `any`, `route`: [`Route`](Js.md#route)) => `boolean` \| `void`

#### Type declaration

▸ (`a`, `b`, `route`): `boolean` \| `void`

**`Description`**

计算两个元素是否相等时用到的函数组，返回值为false/void时被认为不相等

##### Parameters

| Name | Type |
| :------ | :------ |
| `a` | `any` |
| `b` | `any` |
| `route` | [`Route`](Js.md#route) |

##### Returns

`boolean` \| `void`

#### Defined in

src/common/index.ts:665

___

### IgnoreFunc

Ƭ **IgnoreFunc**: (`report`: [`Report`](../interfaces/Js.Report.md)) => `boolean` \| `void`

#### Type declaration

▸ (`report`): `boolean` \| `void`

**`Description`**

用来判断某个route或者值是否被ignore的，如果被ignore了返回true

##### Parameters

| Name | Type |
| :------ | :------ |
| `report` | [`Report`](../interfaces/Js.Report.md) |

##### Returns

`boolean` \| `void`

如果被ignore了返回true

#### Defined in

src/common/index.ts:684

___

### Iterable

Ƭ **Iterable**: { `[key: string | number]`: `any` \| `never`;  } \| `any`[] \| `Set`<`any`\>

任何Iterable类型，包括Object, Set, Array

#### Defined in

src/common/index.ts:599

___

### Route

Ƭ **Route**: (`string` \| `number`)[]

**`Link`**

https://www.lodashjs.com/docs/lodash.set#_setobject-path-value

**`Description`**

Route 向一个Object或者Array内部寻找时使用的键的数组, _.set函数中的path

#### Defined in

src/common/index.ts:620

___

### StopElement

Ƭ **StopElement**: (`obj`: `Object` \| `any`[] \| `Set`<`any`\>) => `boolean` \| `void` \| `Object` \| `any`[] \| `Set`<`any`\>

当寻找该元素或者该函数的返回值为true时就不向深层继续寻找了

#### Defined in

src/common/index.ts:608

___

### StopPositionEle

Ƭ **StopPositionEle**: [`Route`](Js.md#route) \| (`position`: [`Route`](Js.md#route)) => `boolean` \| `void`

当寻找该位置或者该函数的返回值为true时就不向深层继续寻找了

#### Defined in

src/common/index.ts:625

___

### Types

Ƭ **Types**: ``"Object"`` \| ``"String"`` \| ``"Number"`` \| ``"Array"`` \| ``"Undefined"`` \| ``"Null"`` \| ``"Boolean"`` \| ``"Set"`` \| `string`

#### Defined in

src/common/index.ts:586

## Functions

### defaultCompareFunction

▸ **defaultCompareFunction**(`a`, `b`): `boolean`

用来默认判断两个值是否相等的函数

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `a` | `any` | 值a |
| `b` | `any` | 值b |

#### Returns

`boolean`

a和b是否相等

#### Defined in

src/common/index.ts:774

___

### hasArrBefore

▸ **hasArrBefore**(`routeIn`, `origin`): `number`

在route中是否存在上级Array（不考虑源本身） (键为数字的上级)

#### Parameters

| Name | Type |
| :------ | :------ |
| `routeIn` | [`Route`](Js.md#route) |
| `origin` | `Object` |

#### Returns

`number`

是否存在上级Array

#### Defined in

src/common/index.ts:755

___

### toRoutePath

▸ **toRoutePath**(`route`, `separator?`): `string`

**`Description`**

根据route获得path

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `route` | [`Route`](Js.md#route) | `undefined` |
| `separator` | `string` | `"."` |

#### Returns

`string`

#### Defined in

src/common/index.ts:779
