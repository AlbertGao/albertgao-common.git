[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](CSV.md) / GetCsvParams

# Namespace: GetCsvParams

[CSV](CSV.md).GetCsvParams

## Table of contents

### Interfaces

- [Option](../interfaces/CSV.GetCsvParams.Option.md)
- [Result](../interfaces/CSV.GetCsvParams.Result.md)
