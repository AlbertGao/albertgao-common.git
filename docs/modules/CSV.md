[albert.gao](../README.md) / [Exports](../modules.md) / CSV

# Namespace: CSV

csv相关无需依赖的函数和接口

## Table of contents

### Namespaces

- [CsvInterpolateSync](CSV.CsvInterpolateSync.md)
- [FinalResult](CSV.FinalResult.md)
- [GetCsvParams](CSV.GetCsvParams.md)
- [GetIndexed](CSV.GetIndexed.md)
- [GetNearest2](CSV.GetNearest2.md)
- [StartInterpolation](CSV.StartInterpolation.md)

### Interfaces

- [ColIndexed](../interfaces/CSV.ColIndexed.md)
- [Indexed](../interfaces/CSV.Indexed.md)
- [InterpolateFuncArg](../interfaces/CSV.InterpolateFuncArg.md)
- [InterpolateFuncParam](../interfaces/CSV.InterpolateFuncParam.md)

### Type Aliases

- [InterpolateFunc](CSV.md#interpolatefunc)

### Functions

- [csvInterpolateSync](CSV.md#csvinterpolatesync)
- [defaultInterpolate](CSV.md#defaultinterpolate)
- [getAllFrames](CSV.md#getallframes)
- [getCsvParams](CSV.md#getcsvparams)
- [getIndexedSync](CSV.md#getindexedsync)
- [getNearest2](CSV.md#getnearest2)
- [interpolateRowsSync](CSV.md#interpolaterowssync)
- [round](CSV.md#round)

## Type Aliases

### InterpolateFunc

Ƭ **InterpolateFunc**: (`arg`: [`InterpolateFuncArg`](../interfaces/CSV.InterpolateFuncArg.md)) => `any`

#### Type declaration

▸ (`arg`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `arg` | [`InterpolateFuncArg`](../interfaces/CSV.InterpolateFuncArg.md) |

##### Returns

`any`

#### Defined in

src/common/index.ts:1519

## Functions

### csvInterpolateSync

▸ **csvInterpolateSync**(`option`): [`Row`](../interfaces/CSV.FinalResult.Row.md)[]

读取.csv文件并转化为帧数据 (同步)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `option` | [`Option`](../interfaces/CSV.CsvInterpolateSync.Option.md) | 设置 |

#### Returns

[`Row`](../interfaces/CSV.FinalResult.Row.md)[]

读取结果

#### Defined in

src/common/index.ts:1832

___

### defaultInterpolate

▸ **defaultInterpolate**(`arg`): `any`

#### Parameters

| Name | Type |
| :------ | :------ |
| `arg` | [`InterpolateFuncArg`](../interfaces/CSV.InterpolateFuncArg.md) |

#### Returns

`any`

计算出来的结果

#### Defined in

src/common/index.ts:1519

___

### getAllFrames

▸ **getAllFrames**(`csvParams`): `number`[]

获取计算后应该显示的帧数

#### Parameters

| Name | Type |
| :------ | :------ |
| `csvParams` | [`Result`](../interfaces/CSV.GetCsvParams.Result.md) |

#### Returns

`number`[]

#### Defined in

src/common/index.ts:1818

___

### getCsvParams

▸ **getCsvParams**(`options`): [`Result`](../interfaces/CSV.GetCsvParams.Result.md)

初步读取csv文件参数和数据

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `options` | [`Option`](../interfaces/CSV.GetCsvParams.Option.md) | 读取设置 |

#### Returns

[`Result`](../interfaces/CSV.GetCsvParams.Result.md)

读取结果

#### Defined in

src/common/index.ts:1710

___

### getIndexedSync

▸ **getIndexedSync**(`option`): [`Indexed`](../interfaces/CSV.Indexed.md)

将csv每列的有效帧记录为Object

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `option` | [`Option`](../interfaces/CSV.GetIndexed.Option.md) | 可以直接使用getCsvParams的返回值 |

#### Returns

[`Indexed`](../interfaces/CSV.Indexed.md)

第一层键为列名称，第二层键为非整数

#### Defined in

src/common/index.ts:1775

___

### getNearest2

▸ **getNearest2**(`col`, `index`): [`InterpolateFuncParam`](../interfaces/CSV.InterpolateFuncParam.md)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `col` | [`ColIndexed`](../interfaces/CSV.ColIndexed.md) | 列的indexed数据 |
| `index` | `number` | 要查询的index的数据 |

#### Returns

[`InterpolateFuncParam`](../interfaces/CSV.InterpolateFuncParam.md)

最近的两个键和值

#### Defined in

src/common/index.ts:1797

___

### interpolateRowsSync

▸ **interpolateRowsSync**(`option`): [`Row`](../interfaces/CSV.FinalResult.Row.md)[]

将一段allFrames计算出来的数据转换成一个线性插值好的数组，
主要用于子线程中

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `option` | [`Option`](../interfaces/CSV.StartInterpolation.Option.md) | 选项 |

#### Returns

[`Row`](../interfaces/CSV.FinalResult.Row.md)[]

#### Defined in

src/common/index.ts:1683

___

### round

▸ **round**(`num`, `digits?`): `number`

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `num` | `number` | `undefined` |
| `digits` | `number` | `10` |

#### Returns

`number`

#### Defined in

src/common/index.ts:1614
