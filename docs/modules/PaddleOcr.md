[albert.gao](../README.md) / [Exports](../modules.md) / PaddleOcr

# Namespace: PaddleOcr

paddleOcr 函数返回值相关内容

## Table of contents

### Interfaces

- [OcrResult](../interfaces/PaddleOcr.OcrResult.md)
- [Pos](../interfaces/PaddleOcr.Pos.md)
- [PosRaw](../interfaces/PaddleOcr.PosRaw.md)
- [ResultRow](../interfaces/PaddleOcr.ResultRow.md)
- [ResultRowRaw](../interfaces/PaddleOcr.ResultRowRaw.md)
