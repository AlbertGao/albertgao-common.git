[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](CSV.md) / CsvInterpolateSync

# Namespace: CsvInterpolateSync

[CSV](CSV.md).CsvInterpolateSync

## Table of contents

### Interfaces

- [Option](../interfaces/CSV.CsvInterpolateSync.Option.md)
