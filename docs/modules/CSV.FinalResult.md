[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](CSV.md) / FinalResult

# Namespace: FinalResult

[CSV](CSV.md).FinalResult

## Table of contents

### Interfaces

- [Interpolated](../interfaces/CSV.FinalResult.Interpolated.md)
- [Row](../interfaces/CSV.FinalResult.Row.md)
