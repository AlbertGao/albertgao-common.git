[albert.gao](../README.md) / [Exports](../modules.md) / MathExtra

# Namespace: MathExtra

## Table of contents

### Functions

- [log](MathExtra.md#log)

## Functions

### log

▸ **log**(`num`, `base`): `number`

根据base进行幂运算

**`Example`**

```ts
let a = log(2**5 , 2)
console.log(a) // 5
```

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `num` | `number` | 数字 |
| `base` | `number` | 基数 |

#### Returns

`number`

#### Defined in

src/common/index.ts:1263
