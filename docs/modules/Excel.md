[albert.gao](../README.md) / [Exports](../modules.md) / Excel

# Namespace: Excel

## Table of contents

### Interfaces

- [Distribution](../interfaces/Excel.Distribution.md)
- [Position](../interfaces/Excel.Position.md)

### Type Aliases

- [ExcelData](Excel.md#exceldata)
- [Parser](Excel.md#parser)
- [RawInputPos](Excel.md#rawinputpos)

### Functions

- [cellToXy](Excel.md#celltoxy)
- [colToNum](Excel.md#coltonum)
- [distribute](Excel.md#distribute)
- [numToCol](Excel.md#numtocol)
- [parsePos](Excel.md#parsepos)
- [toPosition](Excel.md#toposition)
- [xyToCell](Excel.md#xytocell)

## Type Aliases

### ExcelData

Ƭ **ExcelData**: { `[key: string]`: (`string` \| `number` \| `never`)[];  } \| (`string` \| `number` \| `never`)[][] \| `string` \| `number` \| `never`

#### Defined in

src/common/index.ts:1384

___

### Parser

Ƭ **Parser**: (`target`: [`Distribution`](../interfaces/Excel.Distribution.md)) => `any` \| `never`

#### Type declaration

▸ (`target`): `any` \| `never`

##### Parameters

| Name | Type |
| :------ | :------ |
| `target` | [`Distribution`](../interfaces/Excel.Distribution.md) |

##### Returns

`any` \| `never`

#### Defined in

src/common/index.ts:1426

___

### RawInputPos

Ƭ **RawInputPos**: { `x`: `string` \| `number` ; `y`: `string` \| `number`  } \| `string`

#### Defined in

src/common/index.ts:1382

## Functions

### cellToXy

▸ **cellToXy**(`cell`): [`Position`](../interfaces/Excel.Position.md)

根据字符串获取单元格位置参数

**`Example`**

```ts
cellToXy("E6") // {x: 5, y: 6, cell:"E6" }
```

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `cell` | `string` | 单元格位置 |

#### Returns

[`Position`](../interfaces/Excel.Position.md)

#### Defined in

src/common/index.ts:1336

___

### colToNum

▸ **colToNum**(`col`): `number`

把列转换成数字 (注意：从1开始)

**`Example`**

```ts
colToNum("A") // 1
```

**`Example`**

```ts
colToNum("z") // 26
```

**`Example`**

```ts
colToNum("AD") // 30
```

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `col` | `string` | "A"或者"ABC"不要有空格，可以支持大写或小写 |

#### Returns

`number`

1->A

#### Defined in

src/common/index.ts:1298

___

### distribute

▸ **distribute**(`position`, `sheet`, `data`, `parser?`): [`Distribution`](../interfaces/Excel.Distribution.md)[]

将Data转换为Excel写入任务

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `position` | [`RawInputPos`](Excel.md#rawinputpos) | 写入的cell的位置 |
| `sheet` | `string` | 要写入哪个sheet |
| `data` | [`ExcelData`](Excel.md#exceldata) | 要写入的data |
| `parser` | [`Parser`](Excel.md#parser) | 将data转换为数字或字符串的函数 |

#### Returns

[`Distribution`](../interfaces/Excel.Distribution.md)[]

返回写入Excel的任务分布

#### Defined in

src/common/index.ts:1436

___

### numToCol

▸ **numToCol**(`num`): `string`

把数字转换成列标

**`Example`**

```ts
numToCol(1)) // "A"
```

**`Example`**

```ts
numToCol(26)) // "Z"
```

**`Example`**

```ts
numToCol(30)) // "AD"
```

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `num` | `number` | 从1开始的数字 (注意：从1开始) |

#### Returns

`string`

1=>"A" 26->Z -> 30->AD

#### Defined in

src/common/index.ts:1278

___

### parsePos

▸ **parsePos**(`pos`): [`Position`](../interfaces/Excel.Position.md)

根据字符串或者行列数获取单元格位置

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `pos` | [`RawInputPos`](Excel.md#rawinputpos) | 单元格位置 |

#### Returns

[`Position`](../interfaces/Excel.Position.md)

#### Defined in

src/common/index.ts:1352

___

### toPosition

▸ **toPosition**(`pos`, `x`, `y`): [`Position`](../interfaces/Excel.Position.md)

计算单元格位置的移动x,y后的位置

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `pos` | [`RawInputPos`](Excel.md#rawinputpos) | 单元格位置 |
| `x` | `number` | 列移动，可以为负数，0表示不移动 |
| `y` | `number` | 行移动，可以为负数，0表示不移动 |

#### Returns

[`Position`](../interfaces/Excel.Position.md)

移动后的位置

#### Defined in

src/common/index.ts:1369

___

### xyToCell

▸ **xyToCell**(`x`, `y`): [`Position`](../interfaces/Excel.Position.md)

根据行号列号计算位置

**`Example`**

```ts
xyToPos(5, 6) // "E6"
```

**`Example`**

```ts
xyToPos(30, "D") // "AD4"
```

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `x` | `string` \| `number` | 列号(数字或字母，从1开始的) |
| `y` | `string` \| `number` | 行号(数字或字母，从1开始的) |

#### Returns

[`Position`](../interfaces/Excel.Position.md)

返回正确的行列位置

#### Defined in

src/common/index.ts:1316
