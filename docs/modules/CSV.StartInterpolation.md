[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](CSV.md) / StartInterpolation

# Namespace: StartInterpolation

[CSV](CSV.md).StartInterpolation

## Table of contents

### Interfaces

- [Option](../interfaces/CSV.StartInterpolation.Option.md)
