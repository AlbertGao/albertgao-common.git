[albert.gao](../README.md) / [Exports](../modules.md) / Element

# Namespace: Element

Element-Plus常用的一些算法

## Table of contents

### Interfaces

- [ElementFilter](../interfaces/Element.ElementFilter.md)

### Type Aliases

- [ElementFilters](Element.md#elementfilters)

### Functions

- [generalFilterHandle](Element.md#generalfilterhandle)
- [generalFilters](Element.md#generalfilters)
- [summaryMethod](Element.md#summarymethod)

## Type Aliases

### ElementFilters

Ƭ **ElementFilters**<`T`\>: { [key in keyof T]: ElementFilter<T[key]\>[] }

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends `Object` |

#### Defined in

src/common/index.ts:1852

## Functions

### generalFilterHandle

▸ **generalFilterHandle**(`value`, `row`, `column`): `boolean`

用于element-ui的el-table的filter-method (直接绑定就可以)

**`Description`**

直接用就可以了

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `any` | No description |
| `row` | `any` | No description |
| `column` | `any` | No description |

#### Returns

`boolean`

No description

#### Defined in

src/common/index.ts:1983

___

### generalFilters

▸ **generalFilters**<`T`\>(`tableData`): [`ElementFilters`](Element.md#elementfilters)<`T`\>

生成el-table的filters属性的函数

**`Link`**

https://element-plus.org/zh-CN/component/table.html#table-%E5%B1%9E%E6%80%A7

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends `Object` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `tableData` | `T`[] |

#### Returns

[`ElementFilters`](Element.md#elementfilters)<`T`\>

返回值可用于element-ui的el-table的filters

#### Defined in

src/common/index.ts:1949

___

### summaryMethod

▸ **summaryMethod**(`option`): (`{
    columns,
    data,
  }`: { `columns`: { `property`: `string`  }[] ; `data`: { `[key: string]`: `any`;  }[]  }) => { `[key: string]`: `string` \| `number`;  }

生成计算平均值或者总值的函数, 用于绑定el-table的summary-method

**`Link`**

https://element-plus.org/zh-CN/component/table.html#table-%E5%B1%9E%E6%80%A7

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `option` | `Object` | 选项 |
| `option.averageFilter?` | (`value`: `any`, `columnName`: `string`) => `any` | 用于判断函数的 |
| `option.format?` | (`sum`: `string` \| `number`, `property`: `string`) => `string` \| `number` | 根据计算出的内容，具体要显示出的内容 |
| `option.na?` | ``"N/A"`` | 如果遇到N/A（本行有普通字符串），所显示的内容 |
| `option.useAverage?` | `boolean` | 是否使用平均值 |
| `option.zero?` | ``""`` | 如果遇到0，所显示的内容 |

#### Returns

`fn`

▸ (`{
    columns,
    data,
  }`): `Object`

##### Parameters

| Name | Type |
| :------ | :------ |
| `{
    columns,
    data,
  }` | `Object` |
| `{
    columns,
    data,
  }.columns` | { `property`: `string`  }[] |
| `{
    columns,
    data,
  }.data` | { `[key: string]`: `any`;  }[] |

##### Returns

`Object`

#### Defined in

src/common/index.ts:1862
