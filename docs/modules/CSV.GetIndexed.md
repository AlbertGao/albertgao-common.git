[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](CSV.md) / GetIndexed

# Namespace: GetIndexed

[CSV](CSV.md).GetIndexed

## Table of contents

### Interfaces

- [Option](../interfaces/CSV.GetIndexed.Option.md)
