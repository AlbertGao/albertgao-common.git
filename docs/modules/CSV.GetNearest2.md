[albert.gao](../README.md) / [Exports](../modules.md) / [CSV](CSV.md) / GetNearest2

# Namespace: GetNearest2

[CSV](CSV.md).GetNearest2

## Table of contents

### Interfaces

- [Result0](../interfaces/CSV.GetNearest2.Result0.md)
- [Result1](../interfaces/CSV.GetNearest2.Result1.md)
- [ResultEmpty](../interfaces/CSV.GetNearest2.ResultEmpty.md)
