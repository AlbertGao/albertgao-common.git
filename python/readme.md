# albertgao_python

## Env

This is a python package instead of a Node.js dependency.

## Requirements

- pandas==1.3.4
- pydash==5.1.0
- xlwings==0.24.9

```sh
pip install -r ./node_modules/albertgao_python/requirements.txt
# or if you're using conda
conda install -r ./node_modules/albertgao_python/requirements.txt
```

## How to use

```python
from node_modules import albertgao_python # import __init__
from node_modules.albertgao_python import xlwings_common # import xlwings_common
```
