import re


def getLevelNumber(level: str) -> int:
    """将飞行标准转为数字，数字越小，级别越高

    Args:
        level (str): 字符串的级别例如"Tb"

    Returns:
        Int: 返回数字
    """
    # F - 一副F3
    # E - F2
    # S - F1
    regs = [r"^C$", r"^Tc$", r"^Tb$", r"^Ta?$", r"^T$", r"^A2b$", r"^A2a?$", r"^A1b$", r"^A1a?$",
            r"^Jb$", r"^Ja$", r"^J$", r"^M$", r"^F5$", r"^F4$", r"^(F3B)$", r"^(F3A?|F)$", r"^(F2B)$", r"^(F2A?|E)",  r"^(F1|S)$", r"^F0$"]
    if level is None:
        return len(regs)+1
    for i in range(0, len(regs)):
        reg = regs[i]
        if(re.match(reg, level, flags=re.I)):
            return i
    else:
        return len(regs)+1
