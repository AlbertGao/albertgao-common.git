# albert.gao.dev

This package does nothing but installing the following dependencies:

## @element-plus/icons-vue

    - 2.0.10
## @types/better-sqlite3

    - 7.6.3
## @types/connect-history-api-fallback

    - 1.3.5
## @types/express

    - 4.17.14
## @types/express-session

    - 1.17.5
## @types/express-ws

    - 3.0.1
## @types/fs-extra

    - 9.0.13
## @types/inquirer

    - 8
## @types/jquery

    - 3.5.14
## @types/klaw-sync

    - 6.0.1
## @types/lodash

    - 4.14.191
## @types/node

    - 18.11.13
## @types/nodemon

    - 1.19.2
## @types/passport

    - 1.0.11
## @types/passport-local

    - 1.0.34
## @types/validator

    - 13.7.10
## @types/ws

    - 8.5.3
## @vitejs/plugin-legacy

    - 3.0.1
## @vitejs/plugin-vue

    - 4.0.0
## @vitejs/plugin-vue-jsx

    - 3.0.0
## axios

    - 1.2.1
## chalk

    - 5.2.0
## concurrently

    - 7.6.0
## electron

    - 22.0.0
## electron-builder

    - 23.6.0
## electron-hmr

    - 1.1.7
## nodemon

    - 2.0.20
## pinia

    - 2.0.28
## pm2

    - 5.2.2
## sass

    - 1.56.2
## typedoc

    - 0.23.21
## typedoc-plugin-markdown

    - 3.14.0
## typescript

    - 4.9.4
## unplugin-auto-import

    - 0.12.0
## unplugin-vue-components

    - 0.22.11
## vite

    - 4.0.0
## vue-router

    - 4.1.6