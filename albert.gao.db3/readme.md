# albert.gao.db3

This package does nothing but installing the following dependencies:

## better-sqlite3

    - 8.0.1
## better-sqlite3-session-store

    - 0.1.0
## express-session

    - 1.17.3
## open

    - 8.4.0
## passport

    - 0.6.0
## passport-local

    - 1.0.0
## reflect-metadata

    - 0.1.13
## typeorm

    - 0.3.11