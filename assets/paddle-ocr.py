import sys
import io
import json
from paddleocr import PaddleOCR

ocr = PaddleOCR(use_angle_cls=True, rec=False)


def receive():
    sys.stdin = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
    return json.loads(sys.stdin.read())


result = ocr.ocr(receive()["imgPath"])

print(
    f"<nodejs-result>{json.dumps(result)}</nodejs-result>"
)
